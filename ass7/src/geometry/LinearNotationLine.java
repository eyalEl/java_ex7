package geometry;

/**
 * @author Amir Hozez 200959336 <amirxs1@gmail.com>
 * @author Eyal Elboim 315711804 <eyal.elboim1@gmail.com>
 */
public class LinearNotationLine {
    private double a, b , c; //the notation of the line
    /**
     * make the linear representation of the line that the function accepts.
     * <p>
     * @param theLine -the line that need to do his liner presentation
     */
    public LinearNotationLine(Line theLine) {
        double x1, y1, x2, y2;
        x1 = theLine.start().getX();
        y1 = theLine.start().getY();
        x2 = theLine.end().getX();
        y2 = theLine.end().getY();
        //calculate the abc
        this.a = y2 - y1;
        this.b = x1 - x2;
        this.c = (this.a * x1) + (this.b * y1);
    }
    /**
     * @return the A Notation
     */
    public double getA() {
        return this.a;
    }
    /**
     * @return the B Notation
     */
    public double getB() {
        return this.b;
    }
    /**
     * @return the C Notation
     */
    public double getC() {
        return this.c;
    }
    /**
     * Calculate the determinate with other Line.
     * <p>
     * @param other -the other line that need the to calculate the det with it.
     * @return the determinate.
     */
    public double getDeterminate(Line other) {
        double a2, b2;
        a2 = other.getLinearNotation().getA();
        b2 = other.getLinearNotation().getB();
        return ((this.a * b2) - (a2 * this.b));
    }
}