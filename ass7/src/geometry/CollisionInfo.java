package geometry;
import interfaces.Collidable;

/**
 * @author Amir Hozez 200959336 <amirxs1@gmail.com>
 * @author Eyal Elboim 315711804 <eyal.elboim1@gmail.com>
 */
public class CollisionInfo {
    private Point colPoint;
    private Collidable obj;
    /**
     * Create new CollisionInfo, that contain the information on the hit.
     * <p>
     * @param collisionPoint - the point of the hit.
     * @param collisionObj - the object that involved in the collision.
     */
    public CollisionInfo(Point collisionPoint, Collidable collisionObj) {
        this.colPoint = collisionPoint;
        this.obj = collisionObj;
    }
    /**
     * @return the point at which the collision occurs.
     */
    public Point collisionPoint() {
        return this.colPoint;
    }
    /**
     * @return -the collidable object involved in the collision.
     */
    public Collidable collisionObject() {
        return this.obj;
    }
}