package geometry;
import java.util.ArrayList;
import java.util.List;
/**
 * @author Amir Hozez 200959336 <amirxs1@gmail.com>
 * @author Eyal Elboim 315711804 <eyal.elboim1@gmail.com>
 */
public class Rectangle {
    private Point upperLeft;
    private double width;
    private double height;
    //the lines start in the upper left point and continue clockwise.
    private Line topSide; //the start point is the upper left point.
    private Line rightSide; //the start point is the upper left point.
    private Line bottomSide; //the start point is the lower right point.
    private Line leftSide; //the start point is the lower right point.
    private boolean ignoreUpperLine = false;
 // .
    /**
     * Create a new rectangle with location and width/height.
     * <p>
     * @param upperLeft the upper-left point of the rectangle.
     * @param width -the width of the rectangle
     * @param height -the height of the rectangle
     */
    public Rectangle(Point upperLeft, double width, double height) {
        this.upperLeft = upperLeft;
        this.width = width;
        this.height = height;
        this.setLineOfRect(); //made the line of the rectangle
    }
    /**
     * made the lines of the rectangle.
     */
    private void setLineOfRect() {
        Point upperRight = new Point(this.upperLeft.getX() + this.width,
                this.upperLeft.getY());
        Point lowerLeft = new Point(this.upperLeft.getX(),
                this.upperLeft.getY() + this.height);
        Point lowerRight = new Point(lowerLeft.getX() + this.width,
                lowerLeft.getY());
        this.topSide = new Line(this.upperLeft, upperRight);
        this.rightSide = new Line(upperRight, lowerRight);
        this.bottomSide = new Line(lowerRight, lowerLeft);
        this.leftSide = new Line(lowerLeft, this.upperLeft);
    }
    /**
     * return a list (possibly empty) that contain the all points that
     *  the line is intersection with the rectangle.
     * <p>
     * check the line with the four line of the rectangle, and every
     *  intersection point add to the list.
     * <p>
     * @param line - the line that need to check with the rectangle.
     * @return a (possibly empty) List of intersection points
     *  with the specified line.
     */
    public List<Point> intersectionPoints(Line line) {
        List<Point>  points = new ArrayList<Point>();
        Point tempPoint;
        tempPoint = line.intersectionWith(this.topSide);
        if (null != tempPoint) {
            if (this.ignoreUpperLine) {
                this.ignoreUpperLine = false;
            } else {
                points.add(tempPoint);
            }
        }
        tempPoint = line.intersectionWith(this.rightSide);
        if (null != tempPoint) {
            points.add(tempPoint);
        }
        tempPoint = line.intersectionWith(this.bottomSide);
        if (null != tempPoint) {
            points.add(tempPoint);
        }
        tempPoint = line.intersectionWith(this.leftSide);
        if (null != tempPoint) {
            points.add(tempPoint);
        }
        return points;
    }
    /**
     * Make the rect ignore the next upper line intersection.
     * This is a workaround for the upper line paddle problem, where the
     * ball enters the paddle and gets stuck there
     */
    public void ignoreNextUpperLine() {
        this.ignoreUpperLine = true;
    }
    /**
     * @return the width of the rectangle.
     */
    public double getWidth() {
        return this.width;
    }
    /**
     * @return the height of the rectangle.
     */
    public double getHeight() {
        return this.height;
    }
    /**
     * @return the upper-left point of the rectangle.
     */
    public Point getUpperLeft() {
        return this.upperLeft;
    }
    /**
     * This method is used to obtain a copy of the upper line of the rectangle.
     * <p>
     * @return a copy of the top line
     */
    public Line getTopLineCopy() {
        Point upperRight = new Point(this.upperLeft.getX() + this.width,
                this.upperLeft.getY());
        return new Line(this.upperLeft, upperRight);
    }
    /**
     * This method is used to obtain a copy of the left line of the rectangle.
     * <p>
     * @return a copy of the top line
     */
    public Line getLeftLineCopy() {
        Point lowerLeft = new Point(this.upperLeft.getX(),
                this.upperLeft.getY() + this.height);
        return new Line(lowerLeft, this.upperLeft);
    }
    /**
     * @return a copy of the bottom line
     */
    public Line getBottomLineCopy() {
        Point left = new Point(this.bottomSide.end().getX(), this.bottomSide.end().getY());
        Point right = new Point(this.bottomSide.start().getX(), this.bottomSide.start().getY());
        return new Line(left, right);
    }
    /**
     * return the velocity after the collision with this block in the
     *  given point with the given velocity.
     * <p>
     * @param collisionPoint -where the collision will be.
     * @param currentVelocity -the velocity of the item that
     *   collision with the item
     * @return the new velocity expected after the hit (based on
     *  the force the object inflicted on it).
     */
    public Velocity hit(Point collisionPoint, Velocity currentVelocity) {
        if (this.topSide.isPointInLine(collisionPoint)
                || this.bottomSide.isPointInLine(collisionPoint)) {
            return new Velocity(currentVelocity.getDx(),
                    -currentVelocity.getDy());
        }
        return new Velocity(-currentVelocity.getDx(), currentVelocity.getDy());
    }
}