package geometry;
/**
 * @author Amir Hozez 200959336 <amirxs1@gmail.com>
 * @author Eyal Elboim 315711804 <eyal.elboim1@gmail.com>
 */
public class Point {
    private double x; //the X val of the Point
    private double y; //the Y val of the Point

    /**
     *make new point.
     *<p>
     *@param x - the X val.
     *@param y - the Y val.
     */
    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }
    /**
     * return the distance of this point to the other point.
     * <p>
     * by using the square value of this formula
     *      ((x1-x2)*(x1-x2))+((y1-y2)*(y1-y2))
     * @param secPoint - the other point to calculate the distance
     * @return the distance
     */
    public double distance(Point secPoint) {
        return Math.sqrt(Math.pow((this.x - secPoint.x) , 2)
                + Math.pow((this.y - secPoint.y) , 2));
    }
    /**
     * check if this point and the secPoint are equals.
     * <p>
     * if the X and the Y of both of them equals so return true
     *      otherwise return false
     * <p>
     * @param secPoint -the other point
     * @return true is the points are equal, false otherwise
     */
    public boolean equals(Point secPoint) {
        if (this.x == secPoint.x && this.y == secPoint.y) {
            return true;
        }
        return false;
    }
    /**
     * @return - the X val of the point
     */
    public double getX() {
        return this.x;
    }
    /**
     * @return -the Y val of the point
     */
    public double getY() {
        return this.y;
    }
}