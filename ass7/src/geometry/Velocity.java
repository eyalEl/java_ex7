package geometry;

/**
 * @author Amir Hozez 200959336 <amirxs1@gmail.com>
 * @author Eyal Elboim 315711804 <eyal.elboim1@gmail.com>
 */
public class Velocity {
    private static final int ANGLE_OFFSET_FIX = 90;
    private double dx;
    private double dy;
    /**
     * made new velocity.
     * <p>
     * @param dx - the change in the X coordinate.
     * @param dy - the change in the Y coordinate.
     */
    public Velocity(double dx, double dy) {
        this.dx = dx;
        this.dy = dy;
    }
    /**
     * make a new velocity by speed and angle.
     * <p>
     * @param angle - the angle of the movement.
     * @param speed - the speed of the movement.
     * @return the movement.
     */
    public static Velocity fromAngleAndSpeed(double angle, double speed) {
        /*we need to treat angle 0 as angle 270, because the Origin of the graph
            is upper-left and not lower-left */
        double dx = speed * (Math.cos(Math.toRadians(angle - ANGLE_OFFSET_FIX)));
        double dy = speed * (Math.sin(Math.toRadians(angle - ANGLE_OFFSET_FIX)));
        return new Velocity(dx, dy);
     }
    /**
     * @return the val of the dx(the change in the x coordinate).
     */
    public double getDx() {
        return this.dx;
    }
    /**
     * @return the val of the dy(the change in the y coordinate).
     */
    public double getDy() {
        return this.dy;
    }
    /**
     * "move" the point to new location according the dx and dy.
     * <p>
     *  Take a point with position (x,y) and return a new point
     * with position (x+dx, y+dy)
     * <p>
     * @param p -the point that need to "move"
     * @return the new point after the move.
     */
    public Point applyToPoint(Point p) {
        double newX, newY;
        newX = p.getX() + this.dx;
        newY = p.getY() + this.dy;
        Point newPoint = new Point(newX, newY);
        return newPoint;
    }
}