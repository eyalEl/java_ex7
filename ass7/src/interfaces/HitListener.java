package interfaces;

import gameobjects.Ball;
import gameobjects.Block;
/**
 * @author eyal
 */
public interface HitListener {
    // This method is called whenever the beingHit object is hit.
    // The hitter parameter is the Ball that's doing the hitting.
    /**
     * This method is called whenever the beingHit object is hit.
     * <p>
     * @param beingHit -the block that being hit.
     * @param hitter The hitter parameter is the Ball that's doing the hitting.
     */
    void hitEvent(Block beingHit, Ball hitter);
 }