package interfaces;
import animations.GameLevel;
import geometry.Point;
import geometry.Rectangle;
import geometry.Velocity;
import gameobjects.Ball;

/**
 * @author Amir Hozez 200959336 <amirxs1@gmail.com>
 * @author Eyal Elboim 315711804 <eyal.elboim1@gmail.com>
 */
public interface Collidable {
    /**
     * @return the "collision shape" of the object.
     */
    Rectangle getCollisionRectangle();
    /**
     * return the velocity after the collosion with this block.
     * <p>
     * @param hitter - the ball that hit.
     * @param collisionPoint -where the collision will be.
     * @param currentVelocity -the velocity of the item that
     *   collision with the item
     * @return the new velocity expected after the hit (based on
     *  the force the object inflicted on it).
     */
    Velocity hit(Ball hitter, Point collisionPoint, Velocity currentVelocity);
    /**
     * tell to the object to add to the game.
     *<p>
     * @param g -the Game
     */
    void addToGame(GameLevel g);
}