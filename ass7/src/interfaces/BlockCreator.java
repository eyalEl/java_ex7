package interfaces;

import gameobjects.Block;
/**
 * @author eyal
 */
public interface BlockCreator {
    /**
     * Create a block at the specified location.
     * <p>
     * @param xpos -x coordinate.
     * @param ypos - y coordinate.
     * @return -new block that his upper-left point is the get coordinate.
     */
    Block create(int xpos, int ypos);
}
