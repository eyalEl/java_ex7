package interfaces;
/**
 * @author eyal
 */
public interface HitNotifier {
    /**
     * Add hl as a listener to hit events.
     * <p>
     * @param hl - the hit listener that need to know when hit event.
     */
    void addHitListener(HitListener hl);
    /**
     * Remove hl from the list of listeners to hit events.
     * @param hl - the hit listenter that need to remove.
     */
    void removeHitListener(HitListener hl);
 }