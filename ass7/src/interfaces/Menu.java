package interfaces;
/**
 * This interface represents a menu.
 * @author Amir
 *
 * @param <T> - the generic type that the menu supports
 */
public interface Menu<T> extends Animation {
    /**
     * add the specified selection to the menu.
     * a selection is a menu entry.
     * @param key - the hotkey to press
     * @param message - the message to be shown next to the hotkey
     * @param returnVal - the generic return value
     */
    void addSelection(String key, String message, T returnVal);
    /**
     * add the specified submenu to the menu.
     * a submenu is a menu entry that opens anothre menu
     * @param key - the hotkey to press
     * @param message - the message to be shown next to the hotkey
     * @param subMenu - the generic menu to be held as the submenu
     */
    void addSubMenu(String key, String message, Menu<T> subMenu);
    /**
     * returns the next menu to display after this menu.
     * @return the next menu
     */
    Menu<T> getNextMenu();
    /**
     * set the previous menu of the menu.
     * @param ancestor - the previous menu
     */
    void setAncestor(Menu<T> ancestor);
    /**
     * get the current selection of the menu in the form of
     * the genetic type.
     * @return the generic return value
     */
    T getStatus();
}
