package interfaces;
/**
 * this interface represents a task to be run.
 * @author Amir
 *
 * @param <T>
 */
public interface Task<T> {
    /**
     * the generic run method.
     * @return the generic return vallue
     */
    T run();
}
