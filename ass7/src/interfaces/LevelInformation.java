package interfaces;

import gameobjects.AliensContainer;

/**
 * @author eyal
 */
public interface LevelInformation {
    /**
     * @return the paddle speed
     */
    int paddleSpeed();
    /**
     * @return the paddle width.
     */
    int paddleWidth();
    /**
     * @return level name.
     */
    String levelPatternName();
    /**
     * @return a sprite with the background of the level
     */
    Sprite getBackground();
    /**
     * Note This number should be <= blocks.size().
     * @return Number of balls that should be removed before the level is
     *      considered to be "cleared".
     */
    int numberOfAlienToRemove();
    /**
     * @return the alien container.
     */
    AliensContainer getAliens();
    /**
     * reset aliens to start point.
     */
    void resetAliensToStartPoint();
    /**
     * reset new aliens.
     * @param speed - the new speed
     */
    void resetNewAliens(double speed);
    /**
     * @return initial level speed
     */
    double getInitialLevelSpeed();

}
