package interfaces;

import biuoop.DrawSurface;
/**
 * @author eyal
 */
public interface Animation {
    /**
     * do one frame of this animation.
     * <p>
     * @param d - the screen that on him should be draw the objects.
     * @param dt - the time that passed from the last call
     */
    void doOneFrame(DrawSurface d, double dt);
    /**
     * @return return true if the animation should be continue, otherwise return false.
     */
    boolean shouldStop();
}
