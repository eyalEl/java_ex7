package interfaces;
import java.util.List;
import java.util.ArrayList;
import biuoop.DrawSurface;
/**
 * @author Amir Hozez 200959336 <amirxs1@gmail.com>
 * @author Eyal Elboim 315711804 <eyal.elboim1@gmail.com>
 */
public class SpriteCollection {
    private List<Sprite> allSprite;
    /**
     * Create new collection of sprite.
     */
    public SpriteCollection() {
        this.allSprite = new ArrayList<Sprite>();
    }
    /**
     * add new sprite to the collection.
     * <p>
     * @param s - the new sprite
     */
    public void addSprite(Sprite s) {
        this.allSprite.add(s);
    }
    /**
     * remove the get sprite from the list.
     * <p>
     * @param s -the sprite that need to remove from this list
     */
    public void removeSprite(Sprite s) {
        this.allSprite.remove(s);
    }
    /**
     * call timePassed() on all sprites.
     * <p>
     * @param dt - the time that passed from the last call
     */
    public void notifyAllTimePassed(double dt) {
        for (int i = 0; i < this.allSprite.size(); i++) {
            Sprite temp = this.allSprite.get(i);
            temp.timePassed(dt);
        }
    }
    /**
     * call drawOn(d) on all sprites.
     * <p>
     * @param d -the screen
     */
    public void drawAllOn(DrawSurface d) {
        for (int i = 0; i < this.allSprite.size(); i++) {
            Sprite temp = this.allSprite.get(i);
            temp.drawOn(d);
        }
    }
}