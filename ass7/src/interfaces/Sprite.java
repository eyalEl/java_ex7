package interfaces;
import animations.GameLevel;
import biuoop.DrawSurface;
/**
 * @author Amir Hozez 200959336 <amirxs1@gmail.com>
 * @author Eyal Elboim 315711804 <eyal.elboim1@gmail.com>
 */
public interface Sprite {
    /**
     * draw the sprite to the screen.
     * <p>
     * @param d - the screen
     */
    void drawOn(DrawSurface d);
    /**
     * notify the sprite that time has passed.
     * <p>
     * @param dt -the time that passed from the last call
     */
    void timePassed(double dt);
    /**
     * tell to the object to add to the game.
     *<p>
     * @param g -the Game
     */
    void addToGame(GameLevel g);
}
