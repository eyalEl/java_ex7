package gameobjects;
import misc.Counter;
/**
 * @author eyal
 */
public class ScoreIndicator extends DataTopScreen {
    private static final int PART = 2;
    private static final String FIRST_PART_TEXT = "Score: ";
    private Counter scoreCounter;
    private static final int OFFSET = 20;
    /**
     * Create the headline with the scores.
     * <p>
     * @param scoreCounter -the counter of the scores.
     */
    public ScoreIndicator(Counter scoreCounter) {
        super(PART, OFFSET);
        this.scoreCounter = scoreCounter;
    }
    @Override
    protected String getText() {
        return FIRST_PART_TEXT + Integer.toString(this.scoreCounter.getValue());
    }
}