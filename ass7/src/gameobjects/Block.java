package gameobjects;
import geometry.Point;
import geometry.Rectangle;
import geometry.Velocity;
import interfaces.Collidable;
import interfaces.Sprite;
import interfaces.HitNotifier;
import interfaces.HitListener;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import animations.GameLevel;
import biuoop.DrawSurface;
/**
 * @author Amir Hozez 200959336 <amirxs1@gmail.com>
 * @author Eyal Elboim 315711804 <eyal.elboim1@gmail.com>
 */
public class Block implements Collidable, Sprite, HitNotifier {
    //constants
    private Rectangle rectangle;
    private Color color;
    private int numOfHits;
    private List<HitListener> hitListeners = new ArrayList<HitListener>();
    /**
     * Create new block.
     * <p>
     * @param upperLeft -the upper-left point of the block
     * @param width -the width of the block
     * @param height -the height of the block
     * @param color -the color of the block
     * @param hits -numbers of start hits of the block
     */
    public Block(Point upperLeft, double width, double height, Color color,
            int hits) {
        this (new Rectangle(upperLeft, width, height), color, hits);
    }
    /**
     * Create new block.
     * <p>
     * @param rect -the Rectangle that the ball is.
     * @param color - the color of the block.
     * @param hits -numbers of start hits of the block
     */
    public Block(Rectangle rect, Color color, int hits) {
        this.rectangle = rect;
        this.color = color;
        this.numOfHits = hits;
    }
    /**
     * c'tor.
     * @param upperLeft - the upper left point
     * @param width - the width of the block
     * @param height - the height of the block
     * @param hit - hitcount
     */
    public Block(Point upperLeft, double width, double height, int hit) {
        this(upperLeft, width, height, null, hit);
    }
    /**
     * @return the color of the block
     */
    public Color getColor()  {
        return this.color;
    }
    /**
     * @return the Rectangle of the block
     */
    public Rectangle getCollisionRectangle() {
        return this.rectangle;
    }
    /**
     * return the velocity after the hit.
     * Notify the object that we collided with it.
     * <p>
     * @param hitter - the ball that hit this block.
     * @param currentVelocity -the velocity of the hit
     * @param collisionPoint -the collision point
     * @return the velocity after the hit with this block
     */
    public Velocity hit(Ball hitter, Point collisionPoint, Velocity currentVelocity) {
        if (this.numOfHits > 0) {
            this.numOfHits--;
        }
        this.notifyHit(hitter);
        return this.rectangle.hit(collisionPoint, currentVelocity);
    }
    /**
     * notify the all list that hit just happened.
     * <p>
     * @param hitter -the ball that hit this block.
     */
    protected void notifyHit(Ball hitter) {
        // Make a copy of the hitListeners before iterating over them.
        List<HitListener> listeners = new ArrayList<HitListener>(this.hitListeners);
        // Notify all listeners about a hit event:
        for (HitListener hl : listeners) {
            hl.hitEvent(this, hitter);
        }
    }
    /**
     * draw the block on the given DrawSurface.
     * <p>
     * @param surface - the draw surface on which the ball should be drawn on.
     */
    public void drawOn(DrawSurface surface) {
        surface.setColor(this.color);
        surface.fillRectangle((int) this.rectangle.getUpperLeft().getX(),
                (int) this.rectangle.getUpperLeft().getY(),
                (int) this.rectangle.getWidth(),
                (int) this.rectangle.getHeight());
        /*surface.setColor(Color.BLACK);
        surface.drawRectangle((int) this.rectangle.getUpperLeft().getX(),
                (int) this.rectangle.getUpperLeft().getY(),
                (int) this.rectangle.getWidth(),
                (int) this.rectangle.getHeight());*/
    }
    @Override
    /**
     * notify the block that time has passed.
     */
    public void timePassed(double dt) {    }
    /**
     * add the block to the sprite and collidable collections.
     *<p>
     * @param g -the Game
     */
    public void addToGame(GameLevel g) {
        g.addCollidable(this);
        g.addSprite(this);
    }
    /**
     * remove this from the game.
     * <p>
     * @param game -the game
     */
    public void removeFromGame(GameLevel game) {
        game.removeCollidable(this);
        game.removeSprite(this);
    }
    /**
     * @return the number of the current hit points of this blocks.
     */
    public int getHitPoints() {
        return numOfHits;
    }
    @Override
    public void addHitListener(HitListener hl) {
        this.hitListeners.add(hl);
    }
    @Override
    public void removeHitListener(HitListener hl) {
        this.hitListeners.remove(hl);
    }
    /**
     * update the rectangle position.
     * @param rect - the new rect
     */
    protected void updatePlaceRect(Rectangle rect) {
        this.rectangle = rect;
    }
}
