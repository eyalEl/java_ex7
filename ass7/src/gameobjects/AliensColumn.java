package gameobjects;
import interfaces.HitListener;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import misc.GameEnvironment;
import animations.GameLevel;
import geometry.Point;
import geometry.Velocity;
/**
 * @author hozez
 */
public class AliensColumn implements HitListener {
    public static final int NUM_OF_ALIENS = 5;
    private static final int SPACE_BETWEEN_ALIENS = 10;
    private static final int SHOOT_SIZE = 5;
    private static final Color SHOOT_COLOR = Color.GREEN;
    private static final int SHOOT_SPEED = 300;
    private List<Alien> aliens;
    private double leftSide;
    private double rightSide;
    private double initialX;
    /**
     * Create new Alien column and the inner aliens.
     * <p>
     * @param startX - the upper left X coordinate
     * @param startY - the upper left Y coordinate
     */
    public AliensColumn(double startX, double startY) {
        this.aliens = new ArrayList<Alien>();
        for (int i = 0; i < NUM_OF_ALIENS; i++) {
            Point alienPoint = new Point(startX, startY + (i * Alien.HEIGHT)
                    + (i * SPACE_BETWEEN_ALIENS));
            Alien newAlien = new Alien(alienPoint);
            this.aliens.add(newAlien);
            newAlien.addHitListener(this);
        }
        this.leftSide = startX;
        this.rightSide = startX + Alien.WIDTH;
        this.initialX = startX;
    }
    /**
     * add the aliens to the game.
     * <p>
     * @param g -the game
     */
    public void addAllToGame(GameLevel g) {
        for (Alien a :this.aliens) {
            a.addToGame(g);
        }
    }
    /**
     * add the hit listener to the all aliens.
     * <p>
     * @param hl -the hitListener
     */
    public void addAllHitListener(HitListener hl) {
        for (Alien a: this.aliens) {
            a.addHitListener(hl);
        }
    }
    /**
     * Create a shoot from the bottom of the cloumn.
     * <p>
     * @param g - the game
     * @param env - the game Environment
     */
    public void shoot(GameLevel g, GameEnvironment env) {
        Alien a = this.aliens.get(this.aliens.size() - 1);
        Point startPoint = a.getPointForShoot();
        Ball shoot = new Ball(startPoint, SHOOT_SIZE, SHOOT_COLOR);
        shoot.setVelocity(Velocity.fromAngleAndSpeed(Ball.DOWN, SHOOT_SPEED));
        shoot.addToGame(g);
        shoot.setGameEnvironment(env);
     }
    /**
     * set the diffX and diffY.
     * <p>
     * @param diffX - the new diffX
     * @param diffY - the new diffy
     */
    public void setAllDiffXAndDiffY(double diffX, double diffY) {
        for (Alien a: this.aliens) {
            a.setXDiff(diffX);
            a.setYDiff(diffY);
        }
        this.leftSide += diffX;
        this.rightSide += diffX;
    }

    @Override
    public void hitEvent(Block beingHit, Ball hitter) {
        if (beingHit.getHitPoints() == 0) {
            this.aliens.remove(beingHit);
        }
    }
    /**
     * @return the number of aliens that in the column.
     */
    public int numAliens() {
        return this.aliens.size();
    }
    /**
     * @return the right side coordinate of the cloumn
     */
    public double getRightSide() {
        return this.rightSide;
    }
    /**
     * @return the left side coordinate of the column
     */
    public double getLeftSide() {
        return this.leftSide;
    }
    /**
     * @return the bottom of the lower alien
     */
    public double getBottomY() {
        return this.aliens.get(this.numAliens() - 1).bottomY();
    }
    /**
     * reset the aliens to the start point.
     */
    public void resetColumn() {
        for (Alien a: this.aliens) {
            a.resetAlien();
        }
        this.leftSide = this.initialX;
        this.rightSide = this.initialX + Alien.WIDTH;
    }
    /**
     * @return true if the column is empty, otherwise return false
     */
    public boolean isEmpty() {
        return this.aliens.isEmpty();
    }
}
