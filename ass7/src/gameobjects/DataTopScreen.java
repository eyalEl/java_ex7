package gameobjects;

import geometry.Point;
import interfaces.Sprite;

import java.awt.Color;

import animations.GameLevel;
import biuoop.DrawSurface;
/**
 * @author eyal
 */
public abstract class DataTopScreen implements Sprite {
    private static final int FONT_SIZE = 12;
    private static final Color BACKGROUND_COLOR = Color.WHITE;
    private static final Color TEXT_COLOR = Color.BLACK;
    private Point upperLeft;
    private static final int Y_OF_TITLE = 0;
    public static final int HEIGHT = 15;
    public static final int WIDTH = GameLevel.WIDTH / GameLevel.NUM_OF_HEADLINES;
    private int offset;
    /**
     * Create the Data top screen.
     * <p>
     * @param whichPartInScreen -which this part of the headline.
     * @param offset -the offest of the text in printing
     */
    protected DataTopScreen(int whichPartInScreen, int offset) {
        this.upperLeft = new Point((whichPartInScreen - 1) * WIDTH, Y_OF_TITLE);
        this.offset = offset;
    }
    @Override
    public void drawOn(DrawSurface d) {
        d.setColor(BACKGROUND_COLOR);
        d.fillRectangle((int) this.upperLeft.getX(), (int) this.upperLeft.getY(),
                WIDTH, HEIGHT);
        d.setColor(TEXT_COLOR);
        d.drawText((int) (this.upperLeft.getX() + WIDTH / 2 - this.offset),
                HEIGHT - 2 , getText(), FONT_SIZE);
    }
    /**
     * @return the text that should be printing on the screen.
     */
    protected abstract String getText();
    @Override
    /**
     * notify the sprite that time has passed.
     */
    public void timePassed(double dt) { }
    /**
     * tell to the object to add to the game.
     *<p>
     * @param g -the Game
     */
    public void addToGame(GameLevel g) {
        g.addSprite(this);
    }
}
