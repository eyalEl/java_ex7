package gameobjects;
import geometry.Point;
import geometry.Rectangle;
import geometry.Velocity;
import interfaces.Collidable;
import interfaces.HitNotifier;
import interfaces.Sprite;

import java.awt.Color;

import misc.GameEnvironment;
import animations.GameLevel;
import biuoop.DrawSurface;
import static java.lang.Math.min;
/**
 * @author Amir Hozez 200959336 <amirxs1@gmail.com>
 * @author Eyal Elboim 315711804 <eyal.elboim1@gmail.com>
 */
public class Paddle extends Block implements Sprite, Collidable, HitNotifier {
    //constants
    public static final int PADDLE_HEIGHT = 12;
    public static final int PADDLE_WIDTH = 150;
    private static final int MOVEMENT_SPAN = 600;
    private static final Color PADDLE_COLOR = Color.CYAN;
    private biuoop.KeyboardSensor keyboardSensor;
    private Rectangle innerRect;
    private Point upperLeft;
    private double width = PADDLE_WIDTH;
    private int speedForSec = MOVEMENT_SPAN;
    private double speedForFram = MOVEMENT_SPAN;
    private GameEnvironment environment;
    private static final int OFFSET_FOR_SHOOT = -10;
    private static final int SHOOT_SIZE = 3;
    private static final Color SHOOT_COLOR = Color.RED;
    private static final int SHOOT_SPEED = 400;
    private GameLevel game;
    private static final String SHOOT_KEY = "space";
    private boolean canShoot = true;
    private double currentTimeAfterShoot = 0;
    private static final double COOLDOWN_FOR_SHOOT = 0.35;
    private boolean wasHit = false;
    /**
     * Create new paddle.
     * <p>
     * @param startPoint - the upper-left point of the paddle.
     * @param sensor - Keyboard Sensor
     */
    public Paddle(Point startPoint, biuoop.KeyboardSensor sensor) {
        super(null, null, 0);
        this.upperLeft = startPoint;
        this.keyboardSensor = sensor;
        updateInnerRect();
    }
    /**
     * Create new paddle.
     * <p>
     * @param sensor - Keyboard Sensor
     */
    public Paddle(biuoop.KeyboardSensor sensor) {
        super(null, null, 0);
        this.keyboardSensor = sensor;
    }
    /**
     * set paddle location.
     * @param startPoint - the point to put the paddle in
     */
    public void setLocation(Point startPoint) {
        this.upperLeft = startPoint;
        updateInnerRect();
    }
    /**
     * set the width of the paddle.
     * <p>
     * @param newWidth -the new width
     */
    public void setWidth(double newWidth) {
        this.width = newWidth;
        this.setLocation(new Point(GameLevel.WIDTH / 2 - width / 2,
                GameLevel.HEIGHT - PADDLE_HEIGHT));
    }
    /**
     * notify the shoot where the collidbale object on the screen.
     * <p>
     * @param theEnvironment is the all object on the screen, include the
     *  borders.
     */
    public void setGameEnvironment(GameEnvironment theEnvironment) {
        this.environment = theEnvironment;
    }
    /**
     * set the movment speed of the paddle.
     * <p>
     * @param newSpeed -the new speed
     */
    public void setSpeed(int newSpeed) {
        this.speedForSec = newSpeed;
    }
    /**
     * Create the rectangle of the paddle.
     */
    private void updateInnerRect() {
        this.innerRect = new Rectangle(this.upperLeft, this.width,
                PADDLE_HEIGHT);
    }
    /**
     * set wasHit to false.
     */
    public void reset() {
        this.wasHit = false;
    }
    /**
     * move the paddle to the left.
     */
    private void moveLeft() {
        double possibleMovement = min((this.upperLeft.getX()), this.speedForFram);
        Point newPos = new Point(this.upperLeft.getX() - possibleMovement,
                this.upperLeft.getY());
        this.upperLeft = newPos;
        updateInnerRect();
    }
    /**
     * move the paddle to the right.
     */
    private void moveRight() {
        double possibleMovement = min((GameLevel.WIDTH
                - this.upperLeft.getX() - this.width), this.speedForFram);
        Point newPos = new Point(this.upperLeft.getX() + possibleMovement,
                this.upperLeft.getY());
        this.upperLeft = newPos;
        updateInnerRect();
    }
    @Override
    /**
     * let the paddle know that time passed, and it should move.
     */
    public void timePassed(double dt) {
        this.speedForFram = this.speedForSec * dt;
        if (keyboardSensor.isPressed(biuoop.KeyboardSensor.LEFT_KEY)) {
            moveLeft();
        } else if (keyboardSensor.isPressed(biuoop.KeyboardSensor.RIGHT_KEY)) {
            moveRight();
        }
        if (!this.canShoot) {
            this.currentTimeAfterShoot += dt;
            if (this.currentTimeAfterShoot >= COOLDOWN_FOR_SHOOT) {
                this.currentTimeAfterShoot = 0;
                this.canShoot = true;
            }
        } else if (this.keyboardSensor.isPressed(SHOOT_KEY) && this.canShoot) {
            this.shoot();
            this.canShoot = false;
        }
    }
    /**
     * draw the paddle on the screen.
     * <p>
     * @param surface -the screen
     */
    public void drawOn(DrawSurface surface) {
        surface.setColor(PADDLE_COLOR);
        surface.fillRectangle((int) this.innerRect.getUpperLeft().getX(),
                (int) this.innerRect.getUpperLeft().getY(),
                (int) this.innerRect.getWidth(),
                (int) this.innerRect.getHeight());
        surface.setColor(Color.BLACK);
        surface.drawRectangle((int) this.innerRect.getUpperLeft().getX(),
                (int) this.innerRect.getUpperLeft().getY(),
                (int) this.innerRect.getWidth(),
                (int) this.innerRect.getHeight());
    }
    /**
     * getter for was hit member.
     * @return washit
     */
    public boolean wasHit() {
        return this.wasHit;
    }
    /**
     * @return - the Rectangle of the paddle.
     */
    public Rectangle getCollisionRectangle() {
        return this.innerRect;
    }
    @Override
    /**
     * return the velocity after the hit.
     * Notify the object that we collided with it.
     * prevent the ball form getting stuck in the paddle.
     * <p>
     * @param hitter - the ball that hit this paddle.
     * @param currentVelocity -the velocity of the hit
     * @param collisionPoint -the collision point
     * @return the velocity after the hit with this block
     */
    public Velocity hit(Ball hitter, Point collisionPoint, Velocity currentVelocity) {
        this.notifyHit(hitter);
        this.wasHit = true;
        return currentVelocity;
    }
    /**
     * Add this paddle to the game.
     * <p>
     * @param g -the game.
     */
    public void addToGame(GameLevel g) {
        this.game = g;
        g.addCollidable(this);
        g.addSprite(this);
    }
    /**
     * release a shot by creating a ball.
     */
    private void shoot() {
        Point startPoint = new Point(this.getCollisionRectangle().getTopLineCopy().middle().getX(),
                this.getCollisionRectangle().getTopLineCopy().middle().getY() + OFFSET_FOR_SHOOT);
        Ball shoot = new Ball(startPoint, SHOOT_SIZE, SHOOT_COLOR);
        shoot.setVelocity(Velocity.fromAngleAndSpeed(Ball.UP, SHOOT_SPEED));
        shoot.addToGame(this.game);
        shoot.setGameEnvironment(this.environment);
    }
}
