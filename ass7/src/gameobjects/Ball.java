package gameobjects;
import geometry.CollisionInfo;
import geometry.Line;
import geometry.Point;
import geometry.Velocity;
import interfaces.Sprite;

import java.awt.Color;

import misc.GameEnvironment;
import animations.GameLevel;
import biuoop.DrawSurface;
/**
 * @author Amir Hozez 200959336 <amirxs1@gmail.com>
 * @author Eyal Elboim 315711804 <eyal.elboim1@gmail.com>
 */
public class Ball implements Sprite {
    public static final int UP = 0;
    public static final int DOWN = UP + 180;
    private static final double CLOSEST_DIS_TO_OBJ = 0.1;
    private static final int DEFAULT_MOVE = 0;
    private Point center;
    private int size; //radius.
    private Color color;
    private Velocity moveForSec = new Velocity(DEFAULT_MOVE, DEFAULT_MOVE);
    private Velocity moveForFrame =  new Velocity(DEFAULT_MOVE, DEFAULT_MOVE);
    private GameEnvironment environment;
    /**
     * make a new Ball, by sending the correct parameters to the
     * other c'tor.
     * <p>
     * @param x - the X val of the center point.
     * @param y - the Y val of the center point.
     * @param r -the radius.
     * @param color -the color of the ball.
     */
    public Ball(double x, double y, int r, Color color) {
        this (new Point(x, y), r, color);
    }
    /**
     * make a new Ball.
     * <p>
     * @param center -the center point of the ball
     * @param r -the radius.
     * @param color -the color of the ball.
     */
    public Ball(Point center, int r, Color color) {
        this.center  = center;
        this.size =  r;
        this.color = color;
    }
    /**
     * @return the X val of the center point.
     */
    public int getX() {
        return (int) this.center.getX();
    }
    /**
     * @return the Y val of the center point.
     */
    public int getY() {
        return (int) this.center.getY();
    }
    /**
     * @return the size (radius) of the ball.
     */
    public int getSize() {
        return this.size;
    }
    /**
     * @return the color of the ball.
     */
    public Color getColor() {
        return this.color;
    }
    /**
     * draw the ball on the given DrawSurface.
     * <p>
     * @param surface - the draw surface on which the ball should be drawn
     */
    public void drawOn(DrawSurface surface) {
        surface.setColor(this.color);
        surface.fillCircle((int) this.center.getX(),
                (int) this.center.getY(), this.size);
        surface.setColor(Color.BLACK);
        surface.drawCircle((int) this.center.getX(),
                (int) this.center.getY(), this.size);
    }
    /**
     * check if the center of the ball in valid place, if not change it to
     *  valid location.
     *  <p>
     * if the ball is not in the right place, change it to the closest
     *  place on the board.
     * <p>
     * @param height -the height of the board.
     * @param width -the width of the board.
     */
    public void checkPointOnBoard(int height, int width) {
        /*check if the x and y coordinate are in the range of the board, if
         *  not, change them to the nearest place on the board.
         * the X and Y and must be at least far away from the frame
         * the size of the radius.
         */
        double x = this.getX(), y = this.getY(), r = this.getSize();
        boolean needToChange = false;
        if (x < r) { //too close to the left.
            needToChange = true;
            x = r;
        } else if (x + r > width) { //too close to the right.
            needToChange = true;
            x = width - r;
        }
        if (y < r) { //too close to the top.
            needToChange = true;
            y = r;
        } else if (y + r > height) { //too close to the bottom.
            needToChange = true;
            y = height - r;
        }
        if (needToChange) { //if the center need a change
            this.center = new Point(x, y);
        }
    }
    /**
     * set the velocity of the ball.
     * <p>
     * @param v - the velocity that the ball will get.
     */
    public void setVelocity(Velocity v) {
        this.moveForSec = v;
    }
    /**
     * set the velocity of the ball.
     * <p>
     * @param dx - the dx parameter of the new velocity.
     * @param dy - the dy parameter of the new velocity.
     */
    public void setVelocity(double dx, double dy) {
        this.moveForSec = new Velocity(dx, dy);
    }
    /**
     * @return - the velocity of the ball.
     */
    public Velocity getVelocity() {
        return this.moveForSec;
    }
    /**
     * @return the velocity for one frame
     */
    private Velocity getVelocityForFrame() {
        return this.moveForFrame;
    }
    /**
     * set the velocify for one frame.
     * <p>
     * @param v -the new Velocity.
     */
    private void setVelocityForFrame(Velocity v) {
        this.moveForFrame = v;
    }
    /**
     * notify the ball where the collidbale object on the screen.
     * <p>
     * @param theEnvironment is the all object on the screen, include the
     *  borders.
     */
    public void setGameEnvironment(GameEnvironment theEnvironment) {
        this.environment = theEnvironment;
    }
    /**
     * moveForSec the ball to the new location according the ball move.
     */
    public void moveOneStep() {
        Point endTra = this.getVelocityForFrame().applyToPoint(this.center);
        Line trajectory  = new Line(this.center, endTra);
        CollisionInfo theHit = this.environment.
                getClosestCollision(trajectory);
        if (theHit == null) {
            this.center = endTra;
            return;
        }
        Point tempCenter = this.center;
        //move the ball to "almost" the hit point
        while (tempCenter.distance(theHit.collisionPoint())
                > CLOSEST_DIS_TO_OBJ) {
            tempCenter = (new Line(tempCenter,
                    theHit.collisionPoint())).middle();
        }
        theHit.collisionObject().hit(this, theHit.collisionPoint(),
                this.getVelocity());
        this.center = tempCenter;
        /*
         * check if after the movement the ball will hit something else
         *  (it should happened in corners).
         */
        endTra = this.getVelocityForFrame().applyToPoint(tempCenter);
        trajectory  = new Line(tempCenter, endTra);
        theHit = this.environment.getClosestCollision(trajectory);
        if (theHit != null && theHit.collisionPoint().distance(tempCenter)
                > Double.MIN_VALUE) {
            //wait for the next turn to make the move
            return;
        }
        this.center = this.getVelocityForFrame().applyToPoint(tempCenter);
    }
    @Override
    /**
     * notify the ball that time has passed.
     */
    public void timePassed(double dt) {
        Velocity newVel = new Velocity(this.getVelocity().getDx()
                * dt, this.getVelocity().getDy() * dt);
        this.setVelocityForFrame(newVel);
        this.moveOneStep();
    }
    /**
     * add the ball to the sprite collection in the game.
     * <p>
     * @param g -the game
     */
    public void addToGame(GameLevel g) {
        g.addSprite(this);
        g.addBall(this);
    }
    /**
     * remove this ball from the game.
     * <p>
     * @param g -the game
     */
    public void removeFromGame(GameLevel g) {
        g.removeSprite(this);
        g.removeBall(this);
    }
}
