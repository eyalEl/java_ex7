package gameobjects;

import java.awt.Image;
import java.io.IOException;

import javax.imageio.ImageIO;

import geometry.Point;
import geometry.Rectangle;
import geometry.Velocity;
import biuoop.DrawSurface;
/**
 * @author hozez
 */
public class Alien extends Block {
    //definitions of the alien
    public static final double WIDTH = 40;
    public static final double HEIGHT = 30;
    private static final int OFFSET_Y_COR_FOR_SHOOT = 5;
    private static final String IMAGE_SOURCE = "block_images/enemy.png";
    private static final String ERROR_IMAGE_MSG = "Image not found";
    private static final int HIT = 1;
    //
    private Image image = null;
    private double xDiff = 0;
    private double yDiff = 0;
    private Point startPoint;
    /**
     * Create an alien.
     * <p>
     * @param startPoint -the upperLeft point that the alien start there.
     */
    public Alien(Point startPoint) {
        super(startPoint, WIDTH, HEIGHT, HIT);
        try {
            this.image = ImageIO.read(
                    ClassLoader.getSystemClassLoader().getResourceAsStream(IMAGE_SOURCE));
        } catch (IOException e) {
            System.out.println(ERROR_IMAGE_MSG);
        }
        this.startPoint = startPoint;
    }
    /**
     * set the movement in the x coordinate.
     * <p>
     * @param newXDiff - the new movement
     */
    public void setXDiff(double newXDiff) {
        this.xDiff = newXDiff;
    }
    /**
     * set the movement in the y coordinate.
     * <p>
     * @param newYDiff - the new movement
     */
    public void setYDiff(double newYDiff) {
        this.yDiff = newYDiff;
    }
    @Override
    public void drawOn(DrawSurface surface) {
        surface.drawImage((int) this.getCollisionRectangle().getUpperLeft().getX(),
                (int) this.getCollisionRectangle().getUpperLeft().getY(), this.image);
    }
    @Override
    public void timePassed(double dt) {
        Point newUpperLeft = new Point(super.getCollisionRectangle().getUpperLeft().getX() + this.xDiff,
                super.getCollisionRectangle().getUpperLeft().getY() + this.yDiff);
        super.updatePlaceRect(new Rectangle(newUpperLeft, WIDTH, HEIGHT));
    }
    @Override
    public Velocity hit(Ball hitter, Point collisionPoint, Velocity currentVelocity) {
        if (currentVelocity.getDy() < 0) {
            return super.hit(hitter, collisionPoint, currentVelocity);
        }
        this.notifyHit(hitter);
        return super.getCollisionRectangle().hit(collisionPoint, currentVelocity);
    }
    /**
     * @return the point for the shoot
     */
    public Point getPointForShoot() {
        Point p = super.getCollisionRectangle().getBottomLineCopy().middle();
        return new Point(p.getX(), p.getY() + OFFSET_Y_COR_FOR_SHOOT);
    }
    /**
     * @return the Y coordinate of the bottom of the alien.
     */
    public double bottomY() {
        return super.getCollisionRectangle().getBottomLineCopy().middle().getY();
    }
    /**
     * reset the alien to the start point.
     */
    public void resetAlien() {
        super.updatePlaceRect(new Rectangle(this.startPoint, WIDTH, HEIGHT));
    }
}
