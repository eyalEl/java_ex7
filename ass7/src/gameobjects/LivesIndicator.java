package gameobjects;

import misc.Counter;

/**
 * @author eyal
 */
public class LivesIndicator extends DataTopScreen {
    private static final int PART = 1;
    private static final String FIRST_PART_TEXT = "Lives: ";
    private Counter liveCounter;
    private static final int OFFSET = 0;
    /**
     * Create the headline with the number of the lives on it.
     * <p>
     * @param liveCounter the counter of the lives
     */
    public LivesIndicator(Counter liveCounter) {
        super(PART, OFFSET);
        this.liveCounter = liveCounter;
    }
    @Override
    protected String getText() {
        return FIRST_PART_TEXT + Integer.toString(this.liveCounter.getValue());
    }
}
