package gameobjects;

import java.awt.Color;
import java.awt.Image;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

import javax.imageio.ImageIO;

import biuoop.DrawSurface;
import geometry.Point;

/**
 * @author eyal
 */
public class ChangingBlock extends Block {
    private Map<Integer, Color> fillColor;
    private Map<Integer, Image> fillImage;
    private static final String ERROR_IMAGE_MSG = "Image not found";
    private Color border;
    /**
     * Create a changesBlock -block that can change his fill every hit.
     * <p>
     * @param upperLeft -the upper-left point of the block
     * @param width -the width of the block
     * @param height -the height of the block
     * @param numHits -numbers of start hits of the block
     * @param fillColor - Map that the key is hit-number, the value is the fill color
     * @param fillImage - Map that the key is hit-number, the value is the source image for fill
     * @param border - the color of the borders if null the border won't be draw
     */
    public ChangingBlock(Point upperLeft, int width, int height, int numHits,
            Map<Integer, Color> fillColor, Map<Integer, String> fillImage, Color border) {
        super(upperLeft, (double) width, (double) height, null, numHits);
        this.fillColor = fillColor;
        this.fillImage = new TreeMap<Integer, Image>();
        for (Map.Entry<Integer, String> entry : fillImage.entrySet()) {
            Image img = null;
            try {
                img = ImageIO.read(
                        ClassLoader.getSystemClassLoader().getResourceAsStream(entry.getValue()));
                this.fillImage.put(entry.getKey(), img);
            } catch (IOException e) {
                System.out.println(ERROR_IMAGE_MSG);
            }
        }
        this.border = border;
    }
    @Override
    public void drawOn(DrawSurface d) {
        int hp = super.getHitPoints();
        if (this.fillColor.containsKey(hp)) {
            d.setColor(this.fillColor.get(hp));
            d.fillRectangle((int) super.getCollisionRectangle().getUpperLeft().getX(),
                    (int) super.getCollisionRectangle().getUpperLeft().getY(),
                    (int) super.getCollisionRectangle().getWidth(),
                    (int) super.getCollisionRectangle().getHeight());
        } else {
            d.drawImage((int) super.getCollisionRectangle().getUpperLeft().getX(),
                    (int) super.getCollisionRectangle().getUpperLeft().getY(),
                    this.fillImage.get(hp));
        }
        if (this.border != null) {
            d.setColor(border);
            d.drawRectangle((int) super.getCollisionRectangle().getUpperLeft().getX(),
                    (int) super.getCollisionRectangle().getUpperLeft().getY(),
                    (int) super.getCollisionRectangle().getWidth(),
                    (int) super.getCollisionRectangle().getHeight());
        }

    }
}
