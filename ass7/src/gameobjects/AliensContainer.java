package gameobjects;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import misc.GameEnvironment;
import animations.GameLevel;
import biuoop.DrawSurface;
import interfaces.HitListener;
import interfaces.Sprite;

/**
 * this class contains all aliens in the game, and aggregates relevant
 * functionality.
 * @author Elya
 *
 */
public class AliensContainer implements Sprite {
    private static final double COOLDOWN_FOR_SHOOT = 0.5;
    private static final int NUMBER_OF_COLUMNS = 10;
    private static final int SPACE_BETWEEN_COLUMNS = 10;
    private static final double START_X = 0;
    private static final double START_Y = ScoreIndicator.HEIGHT;
    private static final Random RAND = new Random();
    private static final double INCREASE_SPEED = 1.1;
    private static final double LINEBREAK = 20;
    private static final int NUMBER_OF_ALIENS = NUMBER_OF_COLUMNS * AliensColumn.NUM_OF_ALIENS;
    //
    private GameLevel game;
    private double currentTimeAfterShoot = 0;
    private boolean canShoot;
    private List<AliensColumn> colums;
    private double initialSpeed;
    private double diffX;
    private GameEnvironment environment;
    private boolean hitBottomShield = false;
    /**
     * c'tor.
     * @param speed - initial speed
     */
    public AliensContainer(double speed) {
        this.colums = new ArrayList<AliensColumn>();
        for (int i = 0; i < NUMBER_OF_COLUMNS; i++) {
            AliensColumn c = new AliensColumn(START_X + (i * (SPACE_BETWEEN_COLUMNS + Alien.WIDTH)), START_Y);
            this.colums.add(c);
        }
        this.canShoot = true;
        this.initialSpeed = speed;
        this.diffX = speed;
    }
    @Override
    public void drawOn(DrawSurface d) { }
    /**
     * @return - a random collumn of aliens
     */
    private int getRandColumn() {
        return RAND.nextInt(this.colums.size());
    }
    /**
     * notify the shoot where the collidbale object on the screen.
     * <p>
     * @param theEnvironment is the all object on the screen, include the
     *  borders.
     */
    public void setGameEnvironment(GameEnvironment theEnvironment) {
        this.environment = theEnvironment;
    }
    /**
     * make all aliens go down one line and change direction.
     * @param dt - the current dt
     */
    private void setAllLineBreak(double dt) {
        this.diffX = -(this.diffX * INCREASE_SPEED);
        for (AliensColumn column: this.colums) {
            column.setAllDiffXAndDiffY(this.diffX * dt, LINEBREAK);
        }
    }
    /**
     * move the aliens normally.
     * @param dt - current dt
     */
    private void setAllRegMovement(double dt) {
        for (AliensColumn column: this.colums) {
            column.setAllDiffXAndDiffY(this.diffX * dt, 0);
        }
    }
    /**
     * wrapper for the movement functions.
     * @param dt - current dt
     */
    private void setMovment(double dt) {
      //movement
        if (this.diffX > 0) {
            if (this.colums.get(this.colums.size() - 1).getRightSide() + this.diffX * dt
                    >= GameLevel.WIDTH) {
                this.setAllLineBreak(dt);
            } else {
                this.setAllRegMovement(dt);
            }
        } else if (this.diffX < 0) {
            if (this.colums.get(0).getLeftSide() + this.diffX * dt <= 0) {
                this.setAllLineBreak(dt);
            } else {
                this.setAllRegMovement(dt);
            }
        }
    }
    @Override
    public void timePassed(double dt) {
        List<AliensColumn> tempList = new ArrayList<AliensColumn>(this.colums);
        for (AliensColumn column: tempList) {
            if (column.isEmpty()) {
                this.colums.remove(column);
            }
        }
        if (!this.canShoot) {
            this.currentTimeAfterShoot += dt;
            if (this.currentTimeAfterShoot >= COOLDOWN_FOR_SHOOT) {
                this.currentTimeAfterShoot = 0;
                this.canShoot = true;
            }
        } else {
            int columnNum = this.getRandColumn();
            this.colums.get(columnNum).shoot(this.game, this.environment);
            this.canShoot = false;
        }
        this.setMovment(dt);
        for (AliensColumn column: this.colums) {
            if (column.getBottomY() >= GameLevel.SHIELD_Y_TOP_COR) {
                this.hitBottomShield = true;
            }
        }
    }
    /**
     * @return whether the aliens hit the shield
     */
    public boolean didHitShield() {
        return this.hitBottomShield;
    }
    @Override
    public void addToGame(GameLevel g) {
        this.game = g;
        g.addSprite(this);
        for (AliensColumn column: this.colums) {
            column.addAllToGame(g);
        }
    }
    /**
     * add hit listener to all alien collumns.
     * @param hl - the hit listener
     */
    public void addHitListener(HitListener hl) {
        for (AliensColumn column: this.colums) {
            column.addAllHitListener(hl);
        }
    }
    /**
     * reset all alien collumns.
     */
    public void resetAliens() {
        for (AliensColumn column: this.colums) {
            column.resetColumn();
        }
        this.diffX = this.initialSpeed;
        this.hitBottomShield = false;
    }
    /**
     * @return initial number of aliens
     */
    public int initialNumberAliens() {
        return NUMBER_OF_ALIENS;
    }
}
