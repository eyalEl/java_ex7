package gameobjects;
/**
 * @author eyal
 */
public class LevelName extends DataTopScreen {
    private static final int PART = 3;
    private static final String FIRST_PART_TEXT = "Level Name: ";
    private static final int OFFSET = 100;
    private String name;
    /**
     * Create the headline with the name of the level.
     * <p>
     * @param name -the name of the level
     */
    public LevelName(String name) {
        super(PART, OFFSET);
        this.name = name;
    }
    @Override
    protected String getText() {
        return FIRST_PART_TEXT + name;
    }
}