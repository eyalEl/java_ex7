package misc;
/**
 * @author Amir Hozez 200959336 <amirxs1@gmail.com>
 * @author Eyal Elboim 315711804 <eyal.elboim1@gmail.com>
 * @param <T> -generic Class
 */
public class MenuEntry<T> {
    private String hotkey;
    private String msg;
    private T retval;
    /**
     * Create new MenuEntry.
     * <p>
     * @param hotkey -the key to get this retval
     * @param msg -what sould print on the screen
     * @param retval -what will return
     */
    public MenuEntry(String hotkey, String msg, T retval) {
        this.hotkey = hotkey;
        this.msg = msg;
        this.retval = retval;
    }
    /**
     * @return this hotkey
     */
    public String getKey() {
        return this.hotkey;
    }
    /**
     * @return this message
     */
    public String getMsg() {
        return this.msg;
    }
    /**
     * @return this retval
     */
    public T getRetval() {
        return this.retval;
    }
}
