package misc;

import animations.GameLevel;
import gameobjects.Ball;
import gameobjects.Block;
import interfaces.HitListener;
/**
 * this class is responsible for removing shield blocks.
 * @author Eyaloosh
 *
 */
public class ShieldRemover implements HitListener {
    private GameLevel game;
    /**
     * c'tor.
     * @param game the game
     */
    public ShieldRemover(GameLevel game) {
        this.game = game;
    }
    @Override
    public void hitEvent(Block beingHit, Ball hitter) {
        beingHit.removeFromGame(this.game);
    }

}
