package misc;

import animations.GameLevel;
import gameobjects.Ball;
import gameobjects.Block;
import interfaces.HitListener;
/**
 * @author eyal
 */
public class AlienRemover implements HitListener {
    private GameLevel game;
    private Counter removedBlocks;
    /**
     * Create the block remober.
     * <p>
     * @param game -the game that from him the block will remove.
     * @param removedBlocks -the counter of the blocks in the game.
     */
    public AlienRemover(GameLevel game, Counter removedBlocks) {
        this.game = game;
        this.removedBlocks = removedBlocks;
    }
    @Override
    /**
     * Blocks that are hit and reach 0 hit-points should be removed
     * from the game. Remember to remove this listener from the block
     * that is being removed from the game.
     */
    public void hitEvent(Block beingHit, Ball hitter) {
        if (beingHit.getHitPoints() == 0) {
            beingHit.removeHitListener(this);
            beingHit.removeFromGame(this.game);
            this.removedBlocks.decrement();
        }
    }
}
