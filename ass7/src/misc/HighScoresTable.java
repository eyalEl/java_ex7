package misc;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author eyal
 */
public class HighScoresTable implements Serializable {
    private int size;
    private List<ScoreInfo> scores = new ArrayList<ScoreInfo>();
    /**
     * Create an empty high-scores table with the specified size.
     *      The size means that the table holds up to size top scores.
     * <p>
     * @param size -the size of the table.
     */
    public HighScoresTable(int size) {
        this.size = size;
    }
    /**
     * Add a high-score.
     * <p>
     * if the new score is equals to the last score in the tabel,
     *  so the table won't change...
     * <p>
     * @param score -the new score.
     */
    public void add(ScoreInfo score) {
        this.scores.add(score);
        Collections.sort(this.scores);
        if (this.scores.size() > this.size) {
            this.scores.remove(this.size);
        }
    }
    /**
     * @return table size.
     */
    public int size() {
        return this.size;
    }
    /**
     * Return the current high scores.
     * The list is sorted such that the highest scores come first.
     * @return the current high scores.
     */
    public List<ScoreInfo> getHighScores() {
        return this.scores;
    }
    /**
     * return the rank of the get score.
     * <p>
     * return the rank of the current score: where will it
     *      be on the list if added?
     * Rank 1 means the score will be highest on the list.
     * Rank `size` means the score will be lowest.
     * Rank > `size` means the score is too low and will not
     *   be added to the list.
     * <p>
     * @param score -the score that need to check.
     * @return the rank of the get score.
     */
    public int getRank(int score) {
        for (int i = 0; i < this.size; i++) {
            try {
                int currentScore = this.scores.get(i).getScore();
                if (currentScore < score) {
                    return i + 1;
                }
            } catch (IndexOutOfBoundsException e) {
                return i + 1;
            }
        }
        return this.size + 1;
    }
    /**
     * Clears the table.
     */
    public void clear() {
        this.scores.clear();
        this.size = 0;
    }
    /**
     * Load table data from file.
     *   Current table data is cleared.
     * <p>
     * @param filename -the file with the scores.
     * @throws IOException -when there is a problem with the file.
     */
    public void load(File filename) throws IOException {
        this.clear();
        ObjectInputStream in = new ObjectInputStream(new FileInputStream(filename));
        Object obj = null;
        try {
            obj = in.readObject();
            if (obj instanceof HighScoresTable) {
                HighScoresTable score = (HighScoresTable) obj;
                this.scores.addAll(score.getHighScores());
                this.size = score.size();
            }
        } catch (ClassNotFoundException e) {
            in.close();
            throw new IOException();
        }
        in.close();
    }

    // Save table data to the specified file.
    /**
     * Save table data to the specified file.
     * <p>
     * @param filename -the file for saving the data.
     * @throws IOException -when there is a problem with the file.
     */
    public void save(File filename) throws IOException {
        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(filename));
        out.writeObject(this);
        out.close();
    }
    /**
     * Read a table from file and return it.
     * <p>
     * @param filename - the file.
     * @return -the table, If the file does not exist, or there is a problem with
     *  reading it, an empty table is returned.
     */
    @SuppressWarnings("finally")
    public static HighScoresTable loadFromFile(File filename) {
        HighScoresTable hst = new HighScoresTable(0);
        try {
            hst.load(filename);
        } catch (IOException e) {
            System.out.println("error in load");
        } finally {
            return hst;
        }
    }
}
