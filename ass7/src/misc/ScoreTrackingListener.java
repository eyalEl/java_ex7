package misc;

import gameobjects.Alien;
import gameobjects.Ball;
import gameobjects.Block;
import interfaces.HitListener;
/**
 * @author Amir Hozez 200959336 <amirxs1@gmail.com>
 * @author Eyal Elboim 315711804 <eyal.elboim1@gmail.com>
 */
public class ScoreTrackingListener implements HitListener {
    private static final int REGULAR_HIT_POINTS = 5;
    private static final int DESTROY_HIT_POINTS = 10;
    private static final int DESTROY_ALIEN = 100;
    private Counter currentScore;
    /**
     * Create the score hit listener.
     * <p>
     * @param scoreCounter -the score counter
     */
    public ScoreTrackingListener(Counter scoreCounter) {
        this.currentScore = scoreCounter;
     }
    @Override
    public void hitEvent(Block beingHit, Ball hitter) {
        if (beingHit instanceof Alien) {
            currentScore.increase(DESTROY_ALIEN);
            return;
        }
        currentScore.increase(REGULAR_HIT_POINTS);
        if (beingHit.getHitPoints() == 0) {
            currentScore.increase(DESTROY_HIT_POINTS);
        }
    }
}
