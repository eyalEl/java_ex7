package misc;
/**
 * @author eyal
 */
public class Counter {
    private int count;
    /**
     * Create the counter and reset him with the get number.
     * <p>
     * @param number - the start number of the counter.
     */
    public Counter(int number) {
        this.count = number;
    }
    /**
     * add number to current count.
     * <p>
     * @param number - the number that need to add to the count
     */
    public void increase(int number) {
        this.count += number;
    }
    /**
     * subtract number from current count.
     * <p>
     * @param number - the number that need to subtract from the count.
     */
    public void decrease(int number) {
        this.increase(-number);;
    }
    /**
     * @return current count.
     */
    public int getValue() {
        return this.count;
    }
    /**
     * add 1 to the count.
     */
    public void increment() {
        this.increase(1);
    }
    /**
     * remove one of the count.
     */
    public void decrement() {
        this.decrease(1);
    }
}
