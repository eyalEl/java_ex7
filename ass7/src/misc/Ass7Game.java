package misc;

import interfaces.Menu;
import interfaces.Task;

import animations.AnimationRunner;
import animations.MenuAnimation;
import biuoop.GUI;
import levels.GameFlow;
/**
 * @author Amir Hozez 200959336 <amirxs1@gmail.com>
 * @author Eyal Elboim 315711804 <eyal.elboim1@gmail.com>
 */
public class Ass7Game {
    public static final String TITLE = "Arkanoid game";
    public static final int SCREEN_WIDTH = 800;
    public static final int SCREEN_HEIGHT  = 600;
    private static final int EXIT_SUCCESS = 0;
    private GameFlow gameFlow;
    private GUI gui;
    private AnimationRunner runner;
    /**
     * play the game.
     * <p>
     * @param args - the level set
     */
    public void playGame(String[] args) {
        this.gui = new GUI(TITLE, SCREEN_WIDTH, SCREEN_HEIGHT);
        this.runner = new AnimationRunner(gui);
        this.gameFlow = new GameFlow(runner, gui.getKeyboardSensor(), gui.getDialogManager());
        //display menu and get selection
        Menu<Task<Void>> menu = new MenuAnimation<Task<Void>>(gui.getKeyboardSensor());
        //worker menu
        Menu<Task<Void>> currentMenu;
        Task<Void> startGameTask = new Task<Void>() {
            @Override
            public Void run() {
                gameFlow.runLevels();
                gameFlow = new GameFlow(runner, gui.getKeyboardSensor(), gui.getDialogManager());
                return null;
            }
        };
        //generate anonymous tasks
        Task<Void> quitGameTask = new Task<Void>() {

            @Override
            public Void run() {
                gui.close();
                System.exit(EXIT_SUCCESS);
                return null;
            }
        };
        Task<Void> hiScoresTask = new Task<Void>() {

            @Override
            public Void run() {
                gameFlow.showHiScores();
                return null;
            }
        };
        menu.addSelection(MenuAnimation.START_GAME_HOTKEY,
                MenuAnimation.START_GAME_MSG, startGameTask);
        menu.addSelection(MenuAnimation.HISCORES_HOTKEY,
                MenuAnimation.HISCORES_MSG, hiScoresTask);
        menu.addSelection(MenuAnimation.QUIT_GAME_HOTKEY,
                MenuAnimation.QUIT_GAME_MSG, quitGameTask);
        currentMenu = menu;
        while (true) {
            this.runner.run(currentMenu);
            Task<Void> task = currentMenu.getStatus();
            if (task != null) {
                task.run();
            }
            currentMenu = currentMenu.getNextMenu();
        }
    }
    /**
     * main -run the game.
     * @param args none.
     */
    public static void main(String[] args) {
        Ass7Game theGame = new Ass7Game();
        theGame.playGame(args);
    }
}
