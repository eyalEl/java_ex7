package misc;

import animations.GameLevel;
import gameobjects.Ball;
import gameobjects.Block;
import interfaces.HitListener;
/**
 * @author eyal
 */
public class BallRemover implements HitListener {
    private GameLevel game;
    /**
     * Create the ball remover.
     * <p>
     * @param g - the game that from him the ball will remove.
     */
    public BallRemover(GameLevel g) {
        this.game = g;
    }
    @Override
    public void hitEvent(Block beingHit, Ball hitter) {
        hitter.removeFromGame(this.game);
    }
}
