package misc;
import geometry.CollisionInfo;
import interfaces.Collidable;

import java.util.List;
import java.util.ArrayList;

import geometry.Line;
import geometry.Point;
/**
 * @author Amir Hozez 200959336 <amirxs1@gmail.com>
 * @author Eyal Elboim 315711804 <eyal.elboim1@gmail.com>
 */
public class GameEnvironment {
    private List<Collidable> collidables;
    /**
     * Create new game environment that contain every object that the ball
     *  can collide with.
     */
    public GameEnvironment() {
        collidables = new ArrayList<Collidable>();
    }
    /**
     * Add the object to the list of the object that the ball can collide with.
     * <p>
     * @param c -the new object
     */
    public void addCollidable(Collidable c) {
        this.collidables.add(c);
    }
    /**
     * remove the get Collidable from the list.
     * <p>
     * @param c -the Collidable that need to remove from this list
     */
    public void removeCollidable(Collidable c) {
        this.collidables.remove(c);
    }
    /**
     * Assume an object moving from line.start() to line.end().
     * If this object will not collide with any of the collidables
     * in this collection, return null. Else, return the information
     * about the closest collision that is going to occur.
     * <p>
     * @param trajectory - the trajectory of the move of the ball.
     * @return the information about the closest collision that is
     *   going to occur(with CollisionInfo class)
     */
    public CollisionInfo getClosestCollision(Line trajectory) {
        List<Collidable> allCol = new ArrayList<Collidable>();
        /* over the all object and put in new list in what the line is
         * collide with.
         */
        for (int i = 0; i < this.collidables.size(); i++) {
            Collidable tempCol = (Collidable) this.collidables.get(i);
            if (!tempCol.getCollisionRectangle().intersectionPoints(trajectory)
                    .isEmpty()) {
                allCol.add(tempCol);
            }
        }
        /*
         * if the list is empty it'w mean that the line The
         *   line is not interfering with the object.
         */
        if (allCol.isEmpty()) {
            return null;
        }
        //find the closest collision
        Collidable closestCol = (Collidable) allCol.get(0);
        Point closestPoint = trajectory.
                closestIntersectionToStartOfLine(closestCol.
                        getCollisionRectangle());
        for (int i = 1; i < allCol.size(); i++) {
            Collidable tempCol = (Collidable) allCol.get(i);
            Point tempPoint = trajectory.
                    closestIntersectionToStartOfLine(tempCol.
                            getCollisionRectangle());
            if (tempPoint != null
                    && tempPoint.distance(trajectory.start())
                    < closestPoint.distance(trajectory.start())) {
                closestPoint = tempPoint;
                closestCol = tempCol;
            }
        }
        return new CollisionInfo(closestPoint, closestCol);
    }
}