package misc;

import java.io.Serializable;
/**
 * @author Amir Hozez 200959336 <amirxs1@gmail.com>
 * @author Eyal Elboim 315711804 <eyal.elboim1@gmail.com>
 */
public class ScoreInfo implements Comparable<ScoreInfo>, Serializable {
    private String name;
    private int score;
    /**
     * Create new scoreInfo.
     * <p>
     * @param name -the name of the player.
     * @param score -the scores he got.
     */
    public ScoreInfo(String name, int score) {
        this.name = name;
        this.score = score;
    }
    /**
     * @return the name of the player.
     */
    public String getName() {
        return this.name;
    }
    /**
     * @return the score.
     */
    public int getScore() {
        return this.score;
    }
    /**
     * @return return copy of the ScoreInfo
     */
    public ScoreInfo getCopy() {
        ScoreInfo copy = new ScoreInfo(this.name, this.score);
        return copy;
    }
    @Override
    /**
     * sort from the big to the little one...
     */
    public int compareTo(ScoreInfo other) {
        if (this.score < other.getScore()) {
            return 1;
        }
        if (this.score > other.getScore()) {
            return -1;
        }
        return 0;
    }
}
