package levels;

import java.awt.Color;

import animations.GameLevel;
import biuoop.DrawSurface;
import interfaces.Sprite;
/**
 * @author eyal
 */
public class LevelBackgroundColor implements Sprite {
    private Color color;
    /**
     * Create Background with Color.
     * <p>
     * @param color -the color of the background
     */
    public LevelBackgroundColor(Color color) {
        this.color = color;
    }

    @Override
    public void drawOn(DrawSurface d) {
        d.setColor(this.color);
        d.fillRectangle(0, 0, GameLevel.WIDTH, GameLevel.HEIGHT);
    }

    @Override
    public void timePassed(double dt) { }

    @Override
    public void addToGame(GameLevel g) {
        g.addSprite(this);
    }
}
