package levels;

import java.awt.Color;
import java.awt.Image;
import java.io.IOException;

import javax.imageio.ImageIO;

import interfaces.Sprite;
import animations.GameLevel;
import biuoop.DrawSurface;
/**
 * @author eyal
 */
public class LevelBackgroundImage implements Sprite {
    private Image image = null;
    private static final String ERROR_IMAGE_MSG = "Image not found";
    /**
     * Create Background with the get Image.
     * get the image according to the sorce.
     * <p>
     * @param source -the place of the image
     */
    public LevelBackgroundImage(String source) {
        try {
            this.image = ImageIO.read(
                    ClassLoader.getSystemClassLoader().getResourceAsStream(source));
        } catch (IOException e) {
            System.out.println(ERROR_IMAGE_MSG);
        }
    }
    /**
     * Create Background with the get Image.
     * <p>
     * @param image -the image
     */
    public LevelBackgroundImage(Image image) {
        this.image = image;
    }
    @Override
    public void drawOn(DrawSurface d) {
        d.setColor(Color.WHITE);
        d.drawRectangle(0, 0, GameLevel.WIDTH, GameLevel.HEIGHT);
        d.drawImage(0, 0, this.image);
    }

    @Override
    public void timePassed(double dt) { }

    @Override
    public void addToGame(GameLevel g) {
        g.addSprite(this);
    }
}
