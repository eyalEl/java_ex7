package levels;

import java.awt.Color;
import gameobjects.AliensContainer;
import interfaces.LevelInformation;
import interfaces.Sprite;
/**
 * the single level of the game.
 * @author Eyalinka
 *
 */
public class Level implements LevelInformation {
    private static final int PADDLE_SPEED = 360;
    private static final int PADDLE_WIDTH = 100;
    private static final String PATTERN_NAME = "Battle no.";
    private LevelBackgroundColor background;
    private static final Color BACKGROUND_COLOR = Color.BLACK;
    private AliensContainer aliens;
    private double aliensSpeed;
    /**
     * c'tor.
     * @param speed - the initial speed
     */
    public Level(double speed) {
        this.background = new LevelBackgroundColor(BACKGROUND_COLOR);
        this.aliens = new AliensContainer(speed);
        this.aliensSpeed = speed;
    }
    @Override
    public int paddleSpeed() {
        return PADDLE_SPEED;
    }

    @Override
    public int paddleWidth() {
        return PADDLE_WIDTH;
    }

    @Override
    public String levelPatternName() {
        return PATTERN_NAME;
    }

    @Override
    public Sprite getBackground() {
        return this.background;
    }

    @Override
    public int numberOfAlienToRemove() {
        return this.aliens.initialNumberAliens();
    }

    @Override
    public AliensContainer getAliens() {
        return this.aliens;
    }

    @Override
    public void resetAliensToStartPoint() {
        this.aliens.resetAliens();
    }

    @Override
    public void resetNewAliens(double speed) {
        this.aliensSpeed = speed;
        this.aliens = new AliensContainer(speed);
    }
    /**
     * getter for the game speed.
     * @return the game speed
     */
    public double getInitialLevelSpeed() {
        return this.aliensSpeed;
    }
}
