package levels;

import interfaces.LevelInformation;

import java.io.File;
import java.io.IOException;
import animations.AnimationRunner;
import animations.EndScreen;
import animations.GameLevel;
import animations.HighScoresAnimation;
import animations.KeyPressStoppableAnimation;
import biuoop.KeyboardSensor;
import misc.Counter;
import misc.HighScoresTable;
import misc.ScoreInfo;
import biuoop.DialogManager;
/**
 * @author eyal
 */
public class GameFlow {
    private AnimationRunner runner;
    private KeyboardSensor keyboard;
    private static final int LIVES_NUMBER = 3;
    private Counter lifeCounter = new Counter(LIVES_NUMBER);
    private Counter scoreCounter = new Counter(0);
    private DialogManager dialog;
    private static final double INCREASE_SPEED = 1.1;
    //high-scores
    private static final String SCORES_FILE_SOURCE = "highscores.ser";
    private HighScoresTable highScores;
    private static final int SIZE_TABLE = 8;
    //dialog name
    private static final String DIALOG_TITLE = "Name";
    private static final String DIALOG_QUESTION = "What is your name?";
    private static final String DEFAULT_VALUE = "Anonim";
    private static final double INITIAL_SPEED_ALIEN = 75;
    /**
     * Create the game flow.
     * <p>
     * the GAmeFlow class run the some level together
     * @param ar -the animation ruuner of the game
     * @param ks -the keyboard sensor of the game
     * @param dialog -the dialog manager of the gui
     */
    public GameFlow(AnimationRunner ar, KeyboardSensor ks, DialogManager dialog) {
        this.runner = ar;
        this.keyboard = ks;
        File scores = new File(SCORES_FILE_SOURCE);
        this.dialog = dialog;
        if (scores.exists() && !scores.isDirectory()) {
            this.highScores = HighScoresTable.loadFromFile(scores);
        } else {
            try {
                this.highScores = new HighScoresTable(SIZE_TABLE);
                this.highScores.save(scores);
            } catch (IOException e) {
                System.out.println("problem when save");
            }
        }
    }
    /**
     * run the level in the list one by one.
     * <p>
     */
    public void runLevels() {
        int numLevel = 1;
        double currentSpeed = INITIAL_SPEED_ALIEN;
        LevelInformation initialLevel = new Level(INITIAL_SPEED_ALIEN);
        while (this.lifeCounter.getValue() != 0) {
            Counter removableBlocksCounter =  new Counter(initialLevel.numberOfAlienToRemove());
            GameLevel level = new GameLevel(initialLevel, this.keyboard,
                    this.runner, this.scoreCounter , this.lifeCounter, removableBlocksCounter, numLevel);
            level.setAlienSpeed(currentSpeed);
            level.initialize();
            while (removableBlocksCounter.getValue() != 0 && this.lifeCounter.getValue() != 0) {
                level.playOneTurn();
             }
            numLevel++;
            currentSpeed *= INCREASE_SPEED;
        }
        //run the final end screen
       this.runner.run(new KeyPressStoppableAnimation(this.keyboard, EndScreen.END_KEY,
               new EndScreen(this.lifeCounter, this.scoreCounter)));
       if (this.highScores.getRank(this.scoreCounter.getValue()) <= this.highScores.size()) {
           String newHighScoreName = this.dialog.showQuestionDialog(DIALOG_TITLE,
                   DIALOG_QUESTION, DEFAULT_VALUE);
           ScoreInfo newHighScore = new ScoreInfo(newHighScoreName, this.scoreCounter.getValue());
           this.highScores.add(newHighScore);
           try {
               this.highScores.save(new File(SCORES_FILE_SOURCE));
           } catch (IOException e) {
               System.out.println(e.toString());
           }
       }
       this.showHiScores();
       return;
    }
    /**
     * show the high- score table.
     */
    public void showHiScores() {
        this.runner.run(new KeyPressStoppableAnimation(this.keyboard, HighScoresAnimation.END_KEY,
                new HighScoresAnimation(this.highScores)));
        return;
    }
}
