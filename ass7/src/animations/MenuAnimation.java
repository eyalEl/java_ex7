package animations;

import java.awt.Color;
import java.awt.Image;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import misc.MenuEntry;
import biuoop.DrawSurface;
import biuoop.KeyboardSensor;
import interfaces.Animation;
import interfaces.Menu;
/**
 * This class is responsible for the menu display.
 * @author Amir
 * @param <T> the generic type to be passed to Menu
 */
public class MenuAnimation<T> implements Menu<T>, Animation {
    public static final String START_GAME_HOTKEY   = "s";
    public static final String START_GAME_MSG      = "Start a new game!";
    public static final String HISCORES_HOTKEY     = "h";
    public static final String HISCORES_MSG        = "Show High-Score Table!";
    public static final String QUIT_GAME_HOTKEY    = "q";
    public static final String QUIT_GAME_MSG       = "Quit game...";
    public static final String EASY_LEVEL_HOTKEY   = "e";
    public static final String EASY_LEVEL_MSG      = "Easy";
    public static final String HARD_LEVEL_HOTKEY   = "h";
    public static final String HARD_LEVEL_MSG      = "Hard";
    private static final String BACKGROUND_IMG     = "background_images/menu.png";
    private static final String HEADING_MSG        = "Welcome to space-invaders Game. Choose your destiny!";
    private static final String HOTKEY_MENU_FORMAT = "(%s)";
    private static final int X_COR_HOTKEYS = GameLevel.FRAME_SIZE + 20;
    private static final int X_COR_MSGS = X_COR_HOTKEYS + 50;
    private static final int Y_COR_ANCHOR = 80;
    private static final int LIST_ENTRY_OFFSET = 40;
    private static final int ENTRY_TEXT_SIZE = 30;
    private static final int HEADING_LINE_SIZE = 735;
    private List<MenuEntry<T>> entryList = new ArrayList<MenuEntry<T>>();
    private List<MenuEntry<Menu<T>>> subMenuList = new ArrayList<MenuEntry<Menu<T>>>();
    private boolean shouldStop;
    private KeyboardSensor sensor;
    private T retval;
    private Menu<T> nextMenu, prevMenu;
    private Image animImg;
    /**
     * ctor. receives the keyboard sensor as param.
     * @param sensor - the keyboard sensor
     */
    public MenuAnimation(KeyboardSensor sensor) {
        this.shouldStop = false;
        this.sensor = sensor;
        this.nextMenu = this;
        this.prevMenu = null;
        try {
            this.animImg = ImageIO.read(
                    ClassLoader.getSystemClassLoader().getResourceAsStream(BACKGROUND_IMG));
        } catch (IOException e) {
            System.out.println(AnimationRunner.IMAGE_ERROR_MSG);
        }
    }
    @Override
    public void doOneFrame(DrawSurface d, double dt) {
        this.shouldStop = false;
        d.setColor(Color.WHITE);
        d.drawRectangle(0, 0, GameLevel.WIDTH, GameLevel.HEIGHT);
        d.drawImage(0, 0, this.animImg);
        d.setColor(Color.BLACK);
        d.drawText(X_COR_HOTKEYS, Y_COR_ANCHOR - LIST_ENTRY_OFFSET, HEADING_MSG, ENTRY_TEXT_SIZE); //draw name
        d.drawLine(X_COR_HOTKEYS, Y_COR_ANCHOR - LIST_ENTRY_OFFSET ,
                X_COR_HOTKEYS + HEADING_LINE_SIZE, Y_COR_ANCHOR - LIST_ENTRY_OFFSET);
        MenuEntry<Menu<T>> submenu;
        for (int i = 0; i < subMenuList.size(); i++) {
            submenu = subMenuList.get(i);
            d.drawText(X_COR_HOTKEYS, Y_COR_ANCHOR + i * LIST_ENTRY_OFFSET,
                    String.format(HOTKEY_MENU_FORMAT, submenu.getKey()), ENTRY_TEXT_SIZE); //draw name
            d.drawText(X_COR_MSGS, Y_COR_ANCHOR + i * LIST_ENTRY_OFFSET, submenu.getMsg(), ENTRY_TEXT_SIZE);
            if (this.sensor.isPressed(submenu.getKey())) {
                this.shouldStop = true;
                this.nextMenu = submenu.getRetval();
                this.retval = null;
            }
        }
        MenuEntry<T> entry;
        for (int i = 0; i < entryList.size(); i++) {
            entry = entryList.get(i);
            d.drawText(X_COR_HOTKEYS, Y_COR_ANCHOR + i * LIST_ENTRY_OFFSET
                    + LIST_ENTRY_OFFSET * subMenuList.size(),
                    String.format(HOTKEY_MENU_FORMAT, entry.getKey()), ENTRY_TEXT_SIZE); //draw name
            d.drawText(X_COR_MSGS, Y_COR_ANCHOR + i * LIST_ENTRY_OFFSET
                    + LIST_ENTRY_OFFSET * subMenuList.size(), entry.getMsg(), ENTRY_TEXT_SIZE);
            if (this.sensor.isPressed(entry.getKey())) {
                this.shouldStop = true;
                this.retval = entry.getRetval();
                if (this.prevMenu != null) {
                    this.nextMenu = this.prevMenu;
                }
            }
        }
    }

    @Override
    public boolean shouldStop() {
        if (this.shouldStop) {
            this.shouldStop = false;
            return true;
        }
        return false;
    }
    /**
     * set an ancestor to the menu.
     * the ancestor is the previous menu that led to this one.
     * @param ancestor - the ancestor menu
     */
    public void setAncestor(Menu<T> ancestor) {
        this.prevMenu = ancestor;
    }
    @Override
    public void addSelection(String key, String message, T returnVal) {
        MenuEntry<T> entry = new MenuEntry<T>(key, message, returnVal);
        this.entryList.add(entry);
    }

    @Override
    public T getStatus() {
        return this.retval;
    }
    @Override
    public void addSubMenu(String key, String message, Menu<T> subMenu) {
        MenuEntry<Menu<T>> submenuEntry = new MenuEntry<Menu<T>>(key, message, subMenu);
        this.subMenuList.add(submenuEntry);
    }
    /**
     * get the next menu to be displayed.
     * @return the next menu
     */
    public Menu<T> getNextMenu() {
        Menu<T> ret = this.nextMenu;
        this.nextMenu = this;
        return ret;
    }

}
