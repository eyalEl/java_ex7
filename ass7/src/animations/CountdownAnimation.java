package animations;

import java.awt.Color;

import biuoop.DrawSurface;
import biuoop.Sleeper;
import interfaces.Animation;
import interfaces.SpriteCollection;
/**
 * @author eyal
 */
public class CountdownAnimation implements Animation {
    private double numOfSeconds;
    private int countFrom;
    private int current;
    private double timeHasPassed;
    private SpriteCollection gameScreen;
    private Sleeper slp = new Sleeper();
    private static final Color TEXT_COLOR = Color.CYAN;
    private static final int MILLIS_IN_SEC = 1000;
    private static final int TEXT_SIZE = 50;
    /**
     * Create the animation.
     * <p>
     * @param numOfSeconds -number of the second that the animation need to be.
     * @param countFrom -the number that from him the animation need to count to 1.
     * @param gameScreen -the screen that will be after the this animation
     *      - for the user can prefer himself.
     */
    public CountdownAnimation(double numOfSeconds, int countFrom,
                              SpriteCollection gameScreen) {
        this.numOfSeconds = numOfSeconds;
        this.countFrom = countFrom;
        this.current = countFrom;
        this.gameScreen = gameScreen;
        this.timeHasPassed = numOfSeconds / countFrom;
    }
    @Override
    public boolean shouldStop() {
        return (this.current == 0);
    }
    @Override
    public void doOneFrame(DrawSurface d, double dt) {

        this.gameScreen.drawAllOn(d);
        String curstr = this.current + "...";
        d.setColor(TEXT_COLOR);
        d.drawText(d.getWidth() / 2 , d.getHeight() / 2, curstr, TEXT_SIZE);
        this.slp.sleepFor((long) (((this.numOfSeconds / this.countFrom) * MILLIS_IN_SEC) * dt));
        this.timeHasPassed -= dt;
        if (this.timeHasPassed <= dt) {
            this.current--;
            this.timeHasPassed = this.numOfSeconds / countFrom;
        }
    }
 }
