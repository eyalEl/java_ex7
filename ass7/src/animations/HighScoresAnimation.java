package animations;

import java.awt.Color;
import java.util.List;
import misc.HighScoresTable;
import misc.ScoreInfo;
import biuoop.DrawSurface;
import gameobjects.DataTopScreen;
import interfaces.Animation;

/**
 * @author eyal
 */
public class HighScoresAnimation implements Animation {
    private static final Color BACKGROUND = Color.GRAY;
    //names text
    private static final int X_COR_NAMES = GameLevel.FRAME_SIZE + 100;
    private static final String TEXT_HEAD_LIST_NAME = "Names:";
    private static final int LONG_SUBLINE_NAMES = 150;
    //scores text
    private static final int X_COR_SCORES = X_COR_NAMES + 300;
    private static final String TEXT_HEAD_LIST_SCORE = "Scores:";
    private static final int LONG_SUBLINE_SCORES = 150;
    //headline
    private static final int Y_COR_HEADLINES = GameLevel.FRAME_SIZE + DataTopScreen.HEIGHT + 50;
    private static final int TEXT_SIZE_HEADLINE = 50;
    private static final Color COLOR_HEADLINE = Color.RED;
    private static final int OFFSET_HEADLINE_SUBLINE = 10;
    //list of names and scores
    private static final Color COLOR_LIST = Color.BLACK;
    private static final int TEXT_SIZE_LIST = 35;
    private static final int OFFEST_TEXT_LIST = TEXT_SIZE_LIST + 8;
    private static final int START_Y_COR_LIST = Y_COR_HEADLINES
            + OFFSET_HEADLINE_SUBLINE + 50;
    private List<ScoreInfo> scoreInfoList;
    //exit text
    private static final int X_COR_EXIT_TEXT = GameLevel.FRAME_SIZE + 100;
    private static final int Y_COR_EXIT_TEXT = GameLevel.HEIGHT - GameLevel.FRAME_SIZE - 20;
    private static final int SIZE_EXIT_TEXT = 32;
    private static final Color COLOR_EXIT_TEXT = Color.BLACK;
    private static final String PREFIX_CONTINUE_TEXT = "Press ";
    private static final String POSTFIX_CONTINUE_TEXT = " to continue";
    private String endText;
    public static final String END_KEY = "space";
    /**
     * Create HighScore animation.
     * <p>
     * @param scores -the high score table that need to show on the screen
     */
    public HighScoresAnimation(HighScoresTable scores) {
        this.endText = PREFIX_CONTINUE_TEXT + END_KEY + POSTFIX_CONTINUE_TEXT;
        this.scoreInfoList = scores.getHighScores();
    }
    @Override
    public void doOneFrame(DrawSurface d, double dt) {
        d.setColor(BACKGROUND);
        d.fillRectangle(0, 0, GameLevel.WIDTH, GameLevel.HEIGHT);
        //headlines
        d.setColor(COLOR_HEADLINE);
        d.drawText(X_COR_NAMES, Y_COR_HEADLINES, TEXT_HEAD_LIST_NAME, TEXT_SIZE_HEADLINE);
        d.drawText(X_COR_SCORES, Y_COR_HEADLINES , TEXT_HEAD_LIST_SCORE, TEXT_SIZE_HEADLINE);
        d.drawLine(X_COR_NAMES, Y_COR_HEADLINES + OFFSET_HEADLINE_SUBLINE,
                X_COR_NAMES + LONG_SUBLINE_NAMES, Y_COR_HEADLINES + OFFSET_HEADLINE_SUBLINE);
        d.drawLine(X_COR_SCORES, Y_COR_HEADLINES + OFFSET_HEADLINE_SUBLINE ,
                X_COR_SCORES + LONG_SUBLINE_SCORES, Y_COR_HEADLINES + OFFSET_HEADLINE_SUBLINE);
        for (int i = 0; i < this.scoreInfoList.size(); i++) {
            d.setColor(COLOR_LIST);
            d.drawText(X_COR_NAMES, START_Y_COR_LIST + i * OFFEST_TEXT_LIST,
                    this.scoreInfoList.get(i).getName(), TEXT_SIZE_LIST); //draw name
            d.drawText(X_COR_SCORES, START_Y_COR_LIST + i * OFFEST_TEXT_LIST,
                    String.valueOf(this.scoreInfoList.get(i).getScore()), TEXT_SIZE_LIST);
        }
        d.setColor(COLOR_EXIT_TEXT);
        d.drawText(X_COR_EXIT_TEXT, Y_COR_EXIT_TEXT, this.endText, SIZE_EXIT_TEXT);
    }

    @Override
    public boolean shouldStop() {
        return false;
    }

}
