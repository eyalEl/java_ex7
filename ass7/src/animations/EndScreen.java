package animations;

import java.awt.Color;
import java.awt.Image;
import java.io.IOException;

import javax.imageio.ImageIO;

import misc.Counter;
import biuoop.DrawSurface;
import interfaces.Animation;
/**
 * @author eyal
 */
public class EndScreen implements Animation {
    private Counter lifeCounter, scoreCounter;
    private String imgSrc, finalMsg;
    private static final String WIN_TEXT = "You Win! Your score is: %d. You have %d live(s)!";
    private static final String LOSE_TEXT = "Game Over. Your score is: %d";
    private static final String PRESS_SPACE_TO_EXIT = "Press space to continue...";
    private static final int X_COR = 10;
    private static final int TEXT_1_BOTTOM_OFFSET = 40;
    private static final int TEXT_2_BOTTOM_OFFSET = 20;
    private static final int MAIN_TEXT_SIZE = 28;
    private static final int SUBTEXT_SIZE = 20;
    private static final String WIN_IMG_SRC = "background_images/winner.jpg";
    private static final String LOSE_IMG_SRC = "background_images/game_over.png";
    public static final String END_KEY = "space";
    /**
     * Create the end screen.
     * <p>
     * @param lifeCounter -the live counter.
     * @param scoreCounter -the score counter.
     */
    public EndScreen(Counter lifeCounter, Counter scoreCounter) {
       this.lifeCounter = lifeCounter;
       this.scoreCounter = scoreCounter;
    }
    @Override
    public void doOneFrame(DrawSurface d, double dt) {
       if (this.lifeCounter.getValue() == 0) {
           //player lost
           this.imgSrc = LOSE_IMG_SRC;
           this.finalMsg = String.format(LOSE_TEXT, scoreCounter.getValue());
       } else {
           //player won
           this.imgSrc = WIN_IMG_SRC;
           this.finalMsg = String.format(WIN_TEXT, scoreCounter.getValue(), lifeCounter.getValue());
       }
       d.setColor(Color.WHITE);
       d.drawRectangle(0, 0, GameLevel.WIDTH, GameLevel.HEIGHT);
       Image img = null;
       try {
           img = ImageIO.read(ClassLoader.getSystemClassLoader().getResourceAsStream(this.imgSrc));
       } catch (IOException e) {
           System.out.println(AnimationRunner.IMAGE_ERROR_MSG);
       }
       d.drawImage(0, 0, img);
       d.setColor(Color.BLACK);
       d.drawText(X_COR, d.getHeight() - TEXT_1_BOTTOM_OFFSET, finalMsg , MAIN_TEXT_SIZE);
       d.drawText(X_COR, d.getHeight() - TEXT_2_BOTTOM_OFFSET, PRESS_SPACE_TO_EXIT , SUBTEXT_SIZE);
    }
    @Override
    public boolean shouldStop() {
        return false;
    }
}
