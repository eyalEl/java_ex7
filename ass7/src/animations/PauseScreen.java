package animations;
import java.awt.Color;
import biuoop.DrawSurface;
import interfaces.Animation;
/**
 * @author eyal
 */
public class PauseScreen implements Animation {
    private static final String TEXT = "paused -- press space to continue";
    private static final int X_COR = 10;
    private static final int SIZE_TEXT = 32;
    public static final String END_KEY = "space";
    @Override
    public void doOneFrame(DrawSurface d, double dt) {
       d.setColor(Color.BLACK);
       d.drawText(X_COR, d.getHeight() / 2, TEXT , SIZE_TEXT);
    }
    @Override
    public boolean shouldStop() {
        return false;
    }
}
