package animations;
import biuoop.DrawSurface;
import biuoop.KeyboardSensor;
import gameobjects.Ball;
import gameobjects.Block;
import gameobjects.LevelName;
import gameobjects.LivesIndicator;
import gameobjects.Paddle;
import gameobjects.ScoreIndicator;
import geometry.Point;
import interfaces.Animation;
import interfaces.Collidable;
import interfaces.HitListener;
import interfaces.LevelInformation;
import interfaces.Sprite;
import interfaces.SpriteCollection;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import misc.Ass7Game;
import misc.BallRemover;
import misc.AlienRemover;
import misc.Counter;
import misc.GameEnvironment;
import misc.ScoreTrackingListener;
import misc.ShieldRemover;

/**
 * @author Amir Hozez 200959336 <amirxs1@gmail.com>
 * @author Eyal Elboim 315711804 <eyal.elboim1@gmail.com>
 */
public class GameLevel implements Animation {
    //Description of the screen.
    public static final int WIDTH = Ass7Game.SCREEN_WIDTH;
    public static final int HEIGHT = Ass7Game.SCREEN_HEIGHT;
    public static final Color SCREEN_COLOR = Color.GRAY;
    //Description of the blocks of the frame.
    public static final int NUM_OF_HEADLINES = 3;
    public static final int FRAME_SIZE = 25;
    private static final int DEATH_FRAME_BOTTOM_OFFSET = 10;
    private static final Color FRAME_COLOR = Color.GRAY;
    //Description of the regular blocks.
    private static final int FRAME_BLOCK_HITS = 0;
    //Description of the balls.
    public static final int BALL_SPEED = 360;
    //misc constants
    private static final int FINISHED_STAGE_SCORE = 100;
    //Shields
    private static final int NUMBER_OF_SHIELDS = 3;
    public static final int SHIELD_Y_TOP_COR = HEIGHT - 100;
    private static final int X_LEFT_COR = 100;
    private static final int SPACE_BETWEEN_SHIELDS = 90;
    private static final int WIDTH_PARTICLE_SHIELD = 5;
    private static final int HEIGHT_PARTICLE_SHIELD = 5;
    private static final int NUMBER_PARTICLE_SHIELDS_IN_ROW = 28;
    private static final int NUMBER_PARTICLE_SHIELDS_IN_COLUMN = 3;
    private static final Color SHIELD_COLOR = Color.BLUE;

    //members
    private SpriteCollection sprites;
    private GameEnvironment environment;
    private Counter removableBlocksCounter;
    private Counter scoreCounter;
    private HitListener shieldRemover = new ShieldRemover(this);
    private HitListener alienRemover;
    private HitListener ballRemover = new BallRemover(this);
    private HitListener scoreTracker = new ScoreTrackingListener(this.scoreCounter);
    private Counter lifeCounter;
    private Paddle paddle;
    private AnimationRunner runner;
    private boolean running;
    private KeyboardSensor keyboard;
    private LevelInformation levelInfo;
    private int levelNumber;
    private double alienSpeed;
    private List<Ball> ballsList = new ArrayList<Ball>();
    /**
     * create new level game that run the .
     * <p>
     * @param levelInfo -the information of the level.
     * @param keyboard - the keyboard sensor of the game.
     * @param runner -the animation runner.
     * @param scores -the counter of the scores.
     * @param lifes -the counter of the life.
     * @param removableBlocks -the counter of the blokes that need to remove.
     * @param levelNumber - the number of the level
     */
    public GameLevel(LevelInformation levelInfo, KeyboardSensor keyboard,
            AnimationRunner runner, Counter scores, Counter lifes, Counter removableBlocks,
            int levelNumber) {
        this.sprites = new SpriteCollection();
        this.environment = new GameEnvironment();
        this.levelInfo = levelInfo;
        this.runner = runner;
        this.keyboard = keyboard;
        this.lifeCounter = lifes;
        this.removableBlocksCounter = removableBlocks;
        this.alienRemover = new AlienRemover(this, this.removableBlocksCounter);
        this.scoreCounter = scores;
        this.scoreTracker = new ScoreTrackingListener(this.scoreCounter);
        this.levelNumber = levelNumber;
    }
    /**
     * get the initial alien speed.
     * <p>
     * @param speed -the initial alien speed
     */
    public void setAlienSpeed(double speed) {
        this.alienSpeed = speed;
    }
    /**
     * add the given collidable object to the list.
     * <p>
     * @param c -the new collidable object
     */
    public void addCollidable(Collidable c) {
        this.environment.addCollidable(c);
    }
    /**
     * add the given sprite object to the list.
     * <p>
     * @param s -the new sprite object
     */
    public void addSprite(Sprite s) {
        this.sprites.addSprite(s);
    }
    /**
     * remove the get sprite from the list.
     * <p>
     * @param s -the sprite that need to remove from this list
     */
    public void removeSprite(Sprite s) {
        this.sprites.removeSprite(s);
    }
    /**
     * add the given ball to the balls list.
     * @param b - ball
     */
    public void addBall(Ball b) {
        this.ballsList.add(b);
    }
    /**
     * remove given ball from balls list.
     * @param b the ball
     */
    public void removeBall(Ball b) {
        this.ballsList.remove(b);
    }
    /**
     * remove all balls from the sprite lists.
     */
    public void removeBallsFromSprites() {
        for (Ball b : this.ballsList) {
            this.sprites.removeSprite(b);
        }
    }
    /**
     * remove the get Collidable from the list.
     * <p>
     * @param c -the Collidable that need to remove from this list
     */
    public void removeCollidable(Collidable c) {
        this.environment.removeCollidable(c);
    }
    /**
     * set the blocks of the frame.
     */
    private void setBlocksFrame() {
        Sprite score =  new ScoreIndicator(this.scoreCounter);
        score.addToGame(this);
        Sprite lives = new LivesIndicator(this.lifeCounter);
        lives.addToGame(this);
        String levelName = this.levelInfo.levelPatternName() + String.valueOf(this.levelNumber);
        Sprite levName = new LevelName(levelName);
        levName.addToGame(this);
        Block block = new Block(new Point(-FRAME_SIZE, FRAME_SIZE + ScoreIndicator.HEIGHT), FRAME_SIZE, //left frame
               HEIGHT - FRAME_SIZE - ScoreIndicator.HEIGHT, FRAME_COLOR, FRAME_BLOCK_HITS);
        block.addToGame(this);
        block = new Block(new Point(WIDTH, //right frame
                FRAME_SIZE + ScoreIndicator.HEIGHT), FRAME_SIZE, HEIGHT - FRAME_SIZE - ScoreIndicator.HEIGHT,
                FRAME_COLOR, FRAME_BLOCK_HITS);
        block.addToGame(this);
    }
    /**
     * set the bottom death block.
     */
    private void setDeathBlock() {
        Block block = new Block(new Point(0, HEIGHT + DEATH_FRAME_BOTTOM_OFFSET), //bottom frame
                WIDTH, FRAME_SIZE, FRAME_COLOR, FRAME_BLOCK_HITS);
        block.addHitListener(this.ballRemover);
        block.addToGame(this);
        block = new Block(new Point(0, ScoreIndicator.HEIGHT - FRAME_SIZE), WIDTH, FRAME_SIZE, //top frame
                FRAME_COLOR, FRAME_BLOCK_HITS);
        block.addHitListener(this.ballRemover);
        block.addToGame(this);
    }
    /**
     * Create the shields.
     */
    private void setShields() {
        int currentX = X_LEFT_COR, currentY = SHIELD_Y_TOP_COR;
        for (int i = 0; i < NUMBER_OF_SHIELDS; i++) {
            for (int j = 0; j < NUMBER_PARTICLE_SHIELDS_IN_ROW; j++) {
                for (int k = 0; k < NUMBER_PARTICLE_SHIELDS_IN_COLUMN; k++) {
                    Block shield = new Block(new Point(currentX, currentY), WIDTH_PARTICLE_SHIELD,
                            HEIGHT_PARTICLE_SHIELD, SHIELD_COLOR, 1);
                    shield.addHitListener(this.ballRemover);
                    shield.addHitListener(this.shieldRemover);
                    shield.addToGame(this);
                    currentY += HEIGHT_PARTICLE_SHIELD;
                }
                currentX += WIDTH_PARTICLE_SHIELD;
                currentY = SHIELD_Y_TOP_COR;
            }
            currentX += SPACE_BETWEEN_SHIELDS + WIDTH_PARTICLE_SHIELD;
        }
    }
    /**
     *Create the paddle.
     */
    private void createPaddle() {
        this.paddle = new Paddle(this.keyboard);
        this.paddle.setSpeed(this.levelInfo.paddleSpeed());
        this.paddle.setWidth(this.levelInfo.paddleWidth());
        this.paddle.addToGame(this);
        this.paddle.setGameEnvironment(this.environment);
        this.paddle.addHitListener(ballRemover);
    }
    /**
     * check if a life was lost.
     * <p>
     * @return if the paddle was hit
     */
    private boolean isLifeLost() {
        return (this.paddle.wasHit() || this.levelInfo.getAliens().didHitShield());
    }
    /**
     * put the paddle in the middle of the bottom of the screen.
     */
    private void putPaddleInMiddle() {
        Point paddleLocation = new Point((WIDTH - this.levelInfo.paddleWidth()) / 2,
                HEIGHT - Paddle.PADDLE_HEIGHT);
        this.paddle.setLocation(paddleLocation);
    }
    /**
     * Create the Aliens.
     */
    private void setAliens() {
        this.levelInfo.resetNewAliens(this.alienSpeed);
        this.levelInfo.getAliens().addToGame(this);
        this.levelInfo.getAliens().setGameEnvironment(this.environment);
        this.levelInfo.getAliens().addHitListener(this.ballRemover);
        this.levelInfo.getAliens().addHitListener(this.alienRemover);
        this.levelInfo.getAliens().addHitListener(this.scoreTracker);
    }
    /**
     * create the all objects(blocks, ball, paddle).
     */
    public void initialize() {
        this.levelInfo.getBackground().addToGame(this); //add the background
        this.setDeathBlock();
        this.setBlocksFrame(); //Create the blocks for the frames.
        this.setShields();
        this.setAliens();
        this.createPaddle(); //Create the paddle
    }
    /**
     * initialize that need to do every time that the level start.
     */
    public void initLevel() {
        this.putPaddleInMiddle(); //put paddle in middle of screen
        this.paddle.reset();
        this.levelInfo.resetAliensToStartPoint();
        this.removeBallsFromSprites();
    }
    /**
     * start on turn.
     */
    public void playOneTurn() {
        this.initLevel();
        //wait for the user to press a key
        this.runner.run(new CountdownAnimation(2, 3, this.sprites));
        this.running = true;
        // use our runner to run the current animation -- which is one turn of
        // the game.
        this.runner.run(this);
    }
    @Override
    public void doOneFrame(DrawSurface d,  double dt) {
        this.sprites.drawAllOn(d);
        this.sprites.notifyAllTimePassed(dt);
        if (this.keyboard.isPressed("p")) {
            this.runner.run(new KeyPressStoppableAnimation(this.keyboard,
                    PauseScreen.END_KEY, new PauseScreen()));
         }
        if (this.removableBlocksCounter.getValue() == 0) {
            this.scoreCounter.increase(FINISHED_STAGE_SCORE);
            this.running = false;
        } else if (this.isLifeLost()) {
            this.lifeCounter.decrement();
            this.running = false;
        }
    }
    @Override
    public boolean shouldStop() {
        return (!this.running);
    }
}
