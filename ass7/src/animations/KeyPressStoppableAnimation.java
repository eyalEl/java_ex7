package animations;

import biuoop.DrawSurface;
import biuoop.KeyboardSensor;
import interfaces.Animation;
/**
 * @author eyal
 */
public class KeyPressStoppableAnimation implements Animation {
    private String endKey;
    private KeyboardSensor keyboard;
    private Animation anim;
    private Boolean stop;
    private Boolean isAlreadyPressed;
    /**
     * use of decorator pattern, play the get animation and this animation handle the stop.
     * <p>
     * @param sensor -the keyboard.
     * @param key -the end key of the get animation.
     * @param animation -the animation that need to run.
     */
    public KeyPressStoppableAnimation(KeyboardSensor sensor, String key, Animation animation) {
        this.keyboard = sensor;
        this.endKey = key;
        this.anim = animation;
        this.stop = false;
        this.isAlreadyPressed = true;
    }
    @Override
    public void doOneFrame(DrawSurface d, double dt) {
        this.anim.doOneFrame(d, dt);
        if (this.keyboard.isPressed(this.endKey)) {
            if (!isAlreadyPressed) {
                this.stop = true;
            }
        } else {
            this.isAlreadyPressed = false;
        }
    }

    @Override
    public boolean shouldStop() {
        return this.stop;
    }

}
