package animations;
import interfaces.Animation;
import biuoop.DrawSurface;
import biuoop.GUI;
import biuoop.Sleeper;

/**
 * @author eyal
 */
public class AnimationRunner {
    private GUI gui;
    private Sleeper sleeper =  new Sleeper();
  //Timing constants
    private static final double FRAMES_PER_SEC = 60;
    public static final double MILLISEC_PER_FRAME = 1000 / FRAMES_PER_SEC;
    //Error constants
    public static final String IMAGE_ERROR_MSG = "Problem loading image";
    /**
     * Create the animation runner.
     * <p>
     * @param gui - the gui that contain the screen.
     */
    public AnimationRunner(GUI gui) {
        this.gui =  gui;
    }
    /**
     * run the animation.
     * <p>
     * @param animation the animation the needs to be run.
     */
    public void run(Animation animation) {
        while (!animation.shouldStop()) {
            long startTime = System.currentTimeMillis(); // timing
            DrawSurface d = gui.getDrawSurface();
            animation.doOneFrame(d, 1 / FRAMES_PER_SEC);
            gui.show(d);
            long usedTime = System.currentTimeMillis() - startTime;
            long milliSecondLeftToSleep = (long) MILLISEC_PER_FRAME - usedTime;
            if (milliSecondLeftToSleep > 0) {
                this.sleeper.sleepFor(milliSecondLeftToSleep);
            }
       }
    }
 }
